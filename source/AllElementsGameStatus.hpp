#ifndef KLONDIKE_PATTERNS_ALLELEMENTSGAMESTATUS_HPP
#define KLONDIKE_PATTERNS_ALLELEMENTSGAMESTATUS_HPP

#include "stringable.hpp"

#include <string>
#include <sstream>
#include <unordered_map>

class AllElementsGameStatus final
{
public:
    AllElementsGameStatus(void) = default;
    ~AllElementsGameStatus() = default;

    void Add(concepts::stringable status)
    {
        elements.push_back(status);
    }
private:
    std::vector<concepts::stringable> elements;

    friend std::string to_string(AllElementsGameStatus const& allElementsGameStatus)
    {
        std::stringstream ss;

        auto ite = std::begin(allElementsGameStatus.elements);
        ss << "[" << to_string(*ite);
        for (ite = std::next(std::begin(allElementsGameStatus.elements));
            ite != std::end(allElementsGameStatus.elements);
            ++ite
                )
        {
            ss << "," << to_string(*ite);
        }
        ss << "]";
        return ss.str();
    }
};


#endif //KLONDIKE_PATTERNS_ALLELEMENTSGAMESTATUS_HPP
