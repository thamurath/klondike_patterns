#ifndef KLONDIKE_PATTERNS_MODELSTATELEAF_HPP
#define KLONDIKE_PATTERNS_MODELSTATELEAF_HPP

#include "ModelState.hpp"
#include "ElementView.hpp"
#include "ElementStatus.hpp"

#include "GameStateEncoderVisitor.hpp"

#include "stringable.hpp"

class ModelStateLeaf : public ModelState
{
public:
    ModelStateLeaf(ElementView& elementView, ElementStatus cardHeapStatus);
    ~ModelStateLeaf() override;

    void load(TableModel &modelInterface) override;

    ElementStatus GetElementStatus(void) const
    {
        return cardHeapStatus;
    }
    ElementView& GetElementView(void) const
    {
        return *elementView;
    }

    concepts::stringable Accept(GameStateEncoderVisitor& visitor) override
    {
        return visitor.Visit(*this);
    }
private:
    ElementView* elementView;
    ElementStatus cardHeapStatus;

};

#endif //KLONDIKE_PATTERNS_MODELSTATELEAF_HPP
