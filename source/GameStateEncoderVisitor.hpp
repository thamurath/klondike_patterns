#ifndef KLONDIKE_PATTERNS_GAMESTATEENCODERVISITOR_HPP
#define KLONDIKE_PATTERNS_GAMESTATEENCODERVISITOR_HPP



#include "stringable.hpp"

class ModelStateLeaf;
class ModelStateComposite;
class ElementStatus;

class GameStateEncoderVisitor
{
public:

    virtual concepts::stringable Visit(ModelStateLeaf& modelState) = 0;
    virtual concepts::stringable Visit(ModelStateComposite& modelState) = 0;
    virtual concepts::stringable Visit(ElementStatus& elementStatus) = 0;

    virtual ~GameStateEncoderVisitor(void) = default;

};


#endif //KLONDIKE_PATTERNS_GAMESTATEENCODERVISITOR_HPP
