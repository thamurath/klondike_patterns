#ifndef KLONDIKE_ELEMENTVIEWITERATOR_HPP
#define KLONDIKE_ELEMENTVIEWITERATOR_HPP

#include "ElementView.hpp"

class ElementViewIterator final
{
public:
    ~ElementViewIterator(void) = default;
    

    
    //allow movement
    ElementViewIterator(ElementViewIterator&& rhs) = default;
    ElementViewIterator& operator=(ElementViewIterator&& rhs) = default;

    ElementView& operator*();
    ElementView const& operator*() const;
    ElementView* operator->();
    ElementView const* operator->() const;

    ElementViewIterator& operator++()
    {
        // do actual increment
        return *this;
    }

    ElementViewIterator operator++(int)
    {
        ElementViewIterator tmp(*this);
        operator++();
        return tmp;
    }

protected:
    //allow copy
    ElementViewIterator(ElementViewIterator const& rhs) = default;
    ElementViewIterator& operator=(ElementViewIterator const& rhs) = default;
private:
    ElementViewIterator(void) = default;
    friend class ElementViewIteratorAttorney;
};



#endif //KLONDIKE_ELEMENTVIEWITERATOR_HPP
