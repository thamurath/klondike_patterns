#ifndef KLONDIKE_PATTERNS_MODELSTATECOMPOSITE_HPP
#define KLONDIKE_PATTERNS_MODELSTATECOMPOSITE_HPP


#include "ModelState.hpp"
#include <memory>

class ModelStateComposite : public ModelState
{
public:
    ModelStateComposite(void) = default;
    ~ModelStateComposite() override  = default;

    void load(TableModel &modelInterface) override;

    void add(std::unique_ptr<ModelState> modelState);

    concepts::stringable Accept(GameStateEncoderVisitor& visitor) override
    {
        return visitor.Visit(*this);
    }

    auto& GetChilds(void) const
    {
        return childs;
    }
private:
    std::vector<std::unique_ptr<ModelState>> childs;

};
#endif //KLONDIKE_PATTERNS_MODELSTATECOMPOSITE_HPP
