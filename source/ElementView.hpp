#ifndef KLONDIKE_ELEMENTVIEW_HPP
#define KLONDIKE_ELEMENTVIEW_HPP

#include "ElementType.hpp"
#include "Element.hpp"
#include "ViewStatus.hpp"
#include "StatusTranscoder.hpp"

#include <string>
#include <memory>

#include <cassert>

class ElementView : public Element
{
public:
    ElementView(std::string&& id, ElementType&& type);
    ~ElementView(void) override;
    
    // "polymorphic copy constructor"
    virtual ElementView* Clone(void) const = 0;

    //allow movement
    ElementView(ElementView&& rhs) noexcept = default;
    ElementView& operator=(ElementView&& rhs) noexcept = default;


    virtual void Decode(ViewStatus const& status) = 0;
    virtual void ShowStatus(void) = 0;

    using Element::getId;
    using Element::getType;

    void SetStatusTranscoder(StatusTranscoder* statusTranscoder);
    StatusTranscoder& GetStatusTranscoder(void) const;

    std::uint8_t GetNumCards(void) const {
        assert(false);
        return 0;
    }
protected:
    // allow only copy from the derived clases
    ElementView(ElementView const& rhs) = default;
    ElementView& operator=(ElementView const& rhs) = default;
private:
    StatusTranscoder* statusTranscoder;
};

std::string to_string(ElementView *&elementView);

#endif //KLONDIKE_ELEMENTVIEW_HPP
