#ifndef KLONDIKE_PATTERNS_ELEMENTSTATUS_HPP
#define KLONDIKE_PATTERNS_ELEMENTSTATUS_HPP

#include "Card.hpp"

#include "GameStateEncoderVisitable.hpp"

#include <utility>
#include <vector>

class ElementStatus final : public GameStateEncoderVisitable
{
public:
    explicit ElementStatus(std::vector<Card> cards)
            : cards(std::move(cards))
    {

    }
    ~ElementStatus() override = default;

    ElementStatus(ElementStatus const & rhs) = default;
    ElementStatus& operator=(ElementStatus const & rhs) = default;

    ElementStatus(ElementStatus&& rhs) noexcept = default;
    ElementStatus& operator=(ElementStatus&& rhs) noexcept = default;

    std::vector<Card> GetCards(void) const
    {
        return cards;
    }

    concepts::stringable Accept(GameStateEncoderVisitor& visitor) override
    {
        return visitor.Visit(*this);
    }
protected:
private:
    std::vector<Card> cards;
};

#endif //KLONDIKE_PATTERNS_ELEMENTSTATUS_HPP
