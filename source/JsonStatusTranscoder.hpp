#ifndef KLONDIKE_PATTERNS_JSONSTATUSTRANSCODER_HPP
#define KLONDIKE_PATTERNS_JSONSTATUSTRANSCODER_HPP

#include "StatusTranscoder.hpp"

class JsonStatusTranscoder : public StatusTranscoder
{
public:

     ~JsonStatusTranscoder() override = default;

    std::vector<Card> Decode(ViewStatus& status) override ;
    std::unique_ptr<ViewStatus> Encode(std::vector<Card> cards) override ;

};

#endif //KLONDIKE_PATTERNS_JSONSTATUSTRANSCODER_HPP
