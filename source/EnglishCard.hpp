#ifndef KLONDIKE_ENGLISHCARD_HPP
#define KLONDIKE_ENGLISHCARD_HPP

#include "Card.hpp"
#include "EnglishColor.hpp"
#include "EnglishSuit.hpp"
#include "EnglishFigure.hpp"

namespace english_card
{
    class EnglishCard final : public card_concept
    {
    public:
        EnglishCard(const EnglishFigure &figure, const EnglishSuit &suit, const EnglishColor &color)
                : figure(figure)
                , suit(suit)
                , color(color)
        {

        }

        ~EnglishCard(void)  = default;

        //avoid copy
        EnglishCard(EnglishCard const &rhs) = delete;
        EnglishCard &operator=(EnglishCard const &rhs) = delete;

        //allow movement
        EnglishCard(EnglishCard &&rhs) = default;
        EnglishCard &operator=(EnglishCard &&rhs) = default;

        bool BelongsTo(const Suit &suit) const override
        {
            try
            {
                return figure ==  dynamic_cast<EnglishSuit const &>(suit);
            }
            catch(std::bad_cast const& e)
            {
                return false;
            }
        }

        bool BelongsTo(const Color &color) const override
        {
            try
            {
                return color == dynamic_cast<EnglishColor const &>(color);
            }
            catch(std::bad_cast const& e)
            {
                return false;
            }
        }

        bool IsFigure(const Figure &figure) const override
        {
            try
            {
                return figure == dynamic_cast<EnglishFigure const &>(figure);
            }
            catch(std::bad_cast const& e)
            {
                return false;
            }
        }

        // true if we are the next card to the one passed
        bool IsNextTo(card_concept const &card) const override
        {
            try
            {
                auto& ecard =  dynamic_cast<EnglishCard const &>(card);
                return ecard.figure.isNext(figure);
            }
            catch(std::bad_cast const& e)
            {
                return false;
            }
        }

        // true if we are the previous card to the one passed
        bool IsPrevTo(card_concept const &card) const override
        {
            try
            {
                auto& ecard =  dynamic_cast<EnglishCard const &>(card);
                return ecard.figure.isPrev(figure);
            }
            catch(std::bad_cast const& e)
            {
                return false;
            }
        }

        bool IsFirst(void) const override
        {
            return figure.isFirst();
        }

        bool IsLast(void) const override
        {
            return figure.isLast();
        }

        bool IsSameColor(card_concept const &card) const override
        {
            return card.BelongsTo(color);
        }

        bool IsSameSuit(card_concept const &card) const override
        {
            return card.BelongsTo(suit);
        }

        CardView CreateCardView(void) const override
        {
            return {figure.to_str(), suit.to_str(), color.to_str(), ""};
        }

    protected:
    private:
        EnglishFigure const &figure;
        EnglishSuit const &suit;
        EnglishColor const &color;
    };
}

#endif //KLONDIKE_ENGLISHCARD_HPP
