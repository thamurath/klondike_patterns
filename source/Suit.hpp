
#ifndef KLONDIKE_PATTERNS_SUIT_HPP
#define KLONDIKE_PATTERNS_SUIT_HPP


class Suit
{
public:
    virtual ~Suit() = 0;
};
#endif //KLONDIKE_PATTERNS_SUIT_HPP
