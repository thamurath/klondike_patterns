#ifndef KLONDIKE_PATTERNS_ELEMENTVIEWCOLLECTION_HPP
#define KLONDIKE_PATTERNS_ELEMENTVIEWCOLLECTION_HPP

#include <stringable.hpp>

#include <memory>
#include <vector>
#include <string>
#include <iterator>

class ElementView;

template <typename Derived>
class ElementViewCollectionBase final : private Derived
{
    using value_type  = typename Derived::value_type;
public:
    ElementViewCollectionBase(void) = default;
    ElementViewCollectionBase(std::vector<value_type >&& container )
            : elements(std::move(container))
    {

    }

    ~ElementViewCollectionBase() = default;

    ElementViewCollectionBase(ElementViewCollectionBase const& rhs) =delete;
    ElementViewCollectionBase& operator=(ElementViewCollectionBase const& rhs) =delete;

    ElementViewCollectionBase(ElementViewCollectionBase&& rhs) noexcept = default;
    ElementViewCollectionBase& operator=(ElementViewCollectionBase&& rhs) noexcept = default;

    auto at(auto position) const
    {
        return elements.at(position);
    }
    auto reserve(auto size)
    {
        return elements.reserve(size);
    }

    auto& front(void) const
    {
        return elements.front();
    }
    auto size(void) const
    {
        return std::size(elements);
    }

    auto begin(void)
    {
        return std::begin(elements);
    }
    auto begin(void) const
    {
        return std::begin(elements);
    }

    auto end(void)
    {
        return std::end(elements);
    }
    auto end(void) const
    {
        return std::end(elements);
    }

    void push_back(value_type&& value)
    {
        elements.push_back(std::forward<value_type>(value));
        str_elements.push_back(Derived::Get(value));
    }

    operator std::vector<concepts::stringable>() const
    {

        return  str_elements;
    }

private:
    std::vector<value_type> elements;
    std::vector<concepts::stringable> str_elements;
};


 class OwningElementViewCollection
 {
 public:
     using value_type = std::unique_ptr<ElementView>;
     auto Get(value_type& valueType) const
     {
         return valueType.get();
     }
 };

class NotOwningElementViewCollection
{
public:
    using value_type = ElementView*;
    auto Get(value_type& valueType) const
    {
        return valueType;
    }
};
using ElementViewCollection = ElementViewCollectionBase<OwningElementViewCollection>;
using ElementViewCollectionNotOwning = ElementViewCollectionBase<NotOwningElementViewCollection>;

std::string to_string(ElementView* const& elementView);

#endif //KLONDIKE_PATTERNS_ELEMENTVIEWCOLLECTION_HPP
