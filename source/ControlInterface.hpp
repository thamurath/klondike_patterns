#ifndef KLONDIKE_CONTROLINTERFACE_HPP
#define KLONDIKE_CONTROLINTERFACE_HPP

#include "Command.hpp"
#include "TableModel.hpp"
#include "ReturnCode.hpp"

#include <memory>

class ControlInterface final 
{
public:
    explicit ControlInterface(TableModel& tableModel);
    ~ControlInterface(void) = default;
    
    //avoid copy
    ControlInterface(ControlInterface const& rhs) = delete;
    ControlInterface& operator=(ControlInterface const& rhs) = delete;
    
    //avoid movement
    ControlInterface(ControlInterface&& rhs) = delete;
    ControlInterface& operator=(ControlInterface&& rhs) = delete;


    ReturnCode ExecuteCommand(std::unique_ptr<Command> cmd);
protected:
private:

    TableModel& tableModel;
};



#endif //KLONDIKE_CONTROLINTERFACE_HPP
