#ifndef KLONDIKE_PATTERNS_FOUNDATIONMODEL_HPP
#define KLONDIKE_PATTERNS_FOUNDATIONMODEL_HPP

#include "ElementModel.hpp"

class FoundationModel : public ElementModel
{
public:
    using ElementModel::ElementModel;
    ~FoundationModel() override  = default;

    ElementStatus SaveStatus(void) const override;
    void LoadStatus(ElementStatus const &status) override;

    void AddCard(Card &&card) override;
    void ExtractCard(void) override;
    std::optional<Card> GetCard(void) const override ;
    std::vector<Card> GetCards(std::size_t numCards) const override ;
    std::vector<Card> GetAllCards(void) const override ;
};

#endif //KLONDIKE_PATTERNS_FOUNDATIONMODEL_HPP
