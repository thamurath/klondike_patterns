#ifndef KLONDIKE_PATTERNS_ASKFORSTRINGMENU_HPP
#define KLONDIKE_PATTERNS_ASKFORSTRINGMENU_HPP

#include <cinttypes>
#include <string>

class AskForStringMenu
{
public:
    AskForStringMenu(std::uint32_t min_size)
    : min_size(min_size)
    {

    }
    virtual ~AskForStringMenu(void) = default;

    virtual std::string Execute(std::string questionLabel) = 0;

protected:
    std::uint32_t min_size;
};

#endif //KLONDIKE_PATTERNS_ASKFORSTRINGMENU_HPP
