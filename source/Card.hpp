#ifndef KLONDIKE_CARD_HPP
#define KLONDIKE_CARD_HPP

#include "Suit.hpp"
#include "Color.hpp"
#include "Figure.hpp"
#include "CardView.hpp"

#include <better_enums.hpp>

#include <memory>

BETTER_ENUM(FACE, std::uint8_t, UP, DOWN);

struct card_concept
{
    virtual ~card_concept() = default;


    virtual bool IsTurnUp(void) const = 0;
    virtual bool IsTurnDown(void) const = 0;
    virtual void TurnUp(void) = 0;
    virtual void TurnDown(void) = 0;

    virtual bool BelongsTo(Suit const& suit) const = 0;
    virtual bool BelongsTo(Color const& color) const = 0;
    virtual bool IsFigure(Figure const& figure) const = 0;
    virtual bool IsNextTo(card_concept const& card) const = 0;
    virtual bool IsPrevTo(card_concept const& card) const = 0;
    virtual bool IsFirst(void) const = 0;
    virtual bool IsLast(void) const = 0;
    virtual bool IsSameColor(card_concept const& card) const = 0;
    virtual bool IsSameSuit(card_concept const& card) const = 0;

    virtual CardView CreateCardView(void) const = 0;
};

struct copyable_card_concept : public card_concept
{
    virtual std::unique_ptr<copyable_card_concept> clone(void) const = 0; //allow copy
};

class Card : public card_concept
{


    template <typename SpecificCard>
    struct card_model : public copyable_card_concept
    {
        card_model(SpecificCard const& specificCard)
                : specificCard(specificCard)
                , face(FACE::DOWN)
        {

        }
        card_model(SpecificCard&& specificCard)
                : specificCard(std::move(specificCard))
                , face(FACE::UP)
        {

        }
        ~card_model() override = default;

        std::unique_ptr<card_concept> clone(void) const override
        {
            return std::unique_ptr<card_concept>(new card_model(*this));
        }

        bool IsTurnUp(void) const override
        {
            return face == +(FACE::UP);
        }

        bool IsTurnDown(void) const override
        {
            return face == +(FACE::DOWN);
        }

        void TurnUp(void) override
        {
            face = FACE::UP;
        }

        void TurnDown(void) override
        {
            face = FACE::DOWN;
        }

        bool BelongsTo(Suit const &suit) const override
        {
            return specificCard.BelongsTo(suit);
        }

        bool BelongsTo(Color const &color) const override
        {
            return specificCard.BelongsTo(color);
        }

        bool IsFigure(Figure const &figure) const override
        {
            return specificCard.IsFigure(figure);
        }

        bool IsNextTo(Card const &card) const override
        {
            return specificCard.IsNextTo(card);
        }

        bool IsPrevTo(Card const &card) const override
        {
            return specificCard.IsPrevTo(card);
        }

        bool IsFirst(void) const override
        {
            return specificCard.IsFirst();
        }

        bool IsLast(void) const override
        {
            return specificCard.IsLast();
        }

        bool IsSameColor(Card const &card) const override
        {
            return specificCard.IsSameColor(card);
        }

        bool IsSameSuit(Card const &card) const override
        {
            return specificCard.IsSameSuit(card);
        }

        CardView CreateCardView(void) const override
        {
            auto cardView = specificCard.CreateCardview();
            cardView.Face(face);
            return cardView;
        }
    private:
        SpecificCard specificCard;
        FACE face;
    };
public:
    Card(void) = default;
    virtual ~Card(void) = default;

    template <typename Impl>
    Card(Impl&& impl)
            : impl(new card_model<std::decay_t<Impl>>(std::forward<Impl>(impl)))
    {

    }

    template <typename Impl>
    Card& operator=(Impl&& impl)
    {
        impl.reset(new card_model<std::decay_t<Impl>>(std::forward<Impl>(impl)));
        return *this;
    }

    //allow copy -- needed to be able to store Status ... but I do not like it.
    Card(Card const& rhs)
            : impl(rhs.impl->clone())
    {

    }

    Card& operator=(Card const& rhs)
    {
        impl = rhs.impl->clone();
        return *this;
    }

    // allow movement
    Card(Card&& rhs) noexcept = default;
    Card& operator=(Card&& rhs) noexcept = default;


    bool IsTurnUp(void) const override
    {
        return impl->IsTurnUp();
    }

    bool IsTurnDown(void) const override
    {
        return impl->IsTurnDown();
    }

    void TurnUp(void)  override
    {
        impl->TurnUp();
    }

    void TurnDown(void)  override
    {
        impl->TurnDown();
    }

    bool BelongsTo(Suit const &suit) const  override
    {
        return impl->BelongsTo(suit);
    }

    bool BelongsTo(Color const &color) const override
    {
        return impl->BelongsTo(color);
    }

    bool IsFigure(Figure const &figure) const override
    {
        return impl->IsFigure(figure);
    }

    bool IsNextTo(card_concept const &card) const override
    {
        return impl->IsNextTo(card);
    }

    bool IsPrevTo(card_concept const &card) const override
    {
        return impl->IsPrevTo(card);
    }

    bool IsFirst(void) const override
    {
        return impl->IsFirst();
    }

    bool IsLast(void) const override
    {
        return impl->IsLast();
    }

    bool IsSameColor(card_concept const &card) const override
    {
        return impl->IsSameColor(card);
    }

    bool IsSameSuit(card_concept const &card) const override
    {
        return impl->IsSameSuit(card);
    }

    CardView CreateCardView(void) const override
    {
        return impl->CreateCardView();
    }

protected:
private:
    std::unique_ptr<copyable_card_concept> impl;
};
#endif //KLONDIKE_CARD_HPP
