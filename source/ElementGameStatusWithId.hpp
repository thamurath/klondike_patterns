#ifndef KLONDIKE_PATTERNS_ELEMENTGAMESTATUSWITHID_HPP
#define KLONDIKE_PATTERNS_ELEMENTGAMESTATUSWITHID_HPP

#include <string>
#include <utility>
#include <sstream>

class ElementGameStatusWithId
{
public:
    ElementGameStatusWithId(std::string id, concepts::stringable status)
            : id(std::move(id))
            , status(std::move(status))
    {

    }

    std::string GetId(void) const
    {
        return id;
    }
    concepts::stringable GetStatus(void) const
    {
        return status;
    }
private:
    std::string id;
    concepts::stringable status;
};

std::string to_string(ElementGameStatusWithId const& statusWithId)
{
    std::stringstream ss;

    ss << "{id:" << statusWithId.GetId()
       << ", cards:" << to_string(statusWithId.GetStatus())
       << "}";
    return ss.str();
}
#endif //KLONDIKE_PATTERNS_ELEMENTGAMESTATUSWITHID_HPP
