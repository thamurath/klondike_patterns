#ifndef KLONDIKE_PATTERNS_SAVEGAMECOMMAND_HPP
#define KLONDIKE_PATTERNS_SAVEGAMECOMMAND_HPP


#include "Command.hpp"
#include "ElementViewCollection.hpp"

#include <string>

class SaveGameCommand : public Command
{
public:
    SaveGameCommand(std::string filename, ElementViewCollectionNotOwning viewCollection);
    ~SaveGameCommand(void) override = default;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;

private:
    std::string filename;
    ElementViewCollectionNotOwning viewCollection;
};

#endif //KLONDIKE_PATTERNS_SAVEGAMECOMMAND_HPP
