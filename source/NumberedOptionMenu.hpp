#ifndef KLONDIKE_PATTERNS_NUMBEREDOPTIONMENU_HPP
#define KLONDIKE_PATTERNS_NUMBEREDOPTIONMENU_HPP

#include <stringable.hpp>

#include <string>
#include <vector>

class NumberedOptionMenu
{
public:
    explicit NumberedOptionMenu(std::vector<concepts::stringable>&& options);
    virtual ~NumberedOptionMenu() = default;

    virtual std::uint32_t Execute(std::string questionLabel) = 0;

protected:
    std::vector<concepts::stringable> options;
};

#endif //KLONDIKE_PATTERNS_NUMBEREDOPTIONMENU_HPP
