#ifndef KLONDIKE_PATTERNS_COLOR_HPP
#define KLONDIKE_PATTERNS_COLOR_HPP

class Color
{
public:
    virtual ~Color() = 0;
};

#endif //KLONDIKE_PATTERNS_COLOR_HPP
