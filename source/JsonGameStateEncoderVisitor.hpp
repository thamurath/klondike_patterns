#ifndef KLONDIKE_PATTERNS_JSONGAMESTATEENCODERVISITOR_HPP
#define KLONDIKE_PATTERNS_JSONGAMESTATEENCODERVISITOR_HPP

#include "GameStateEncoderVisitor.hpp"

class JsonGameStateEncoderVisitor : public GameStateEncoderVisitor
{
public:
    ~JsonGameStateEncoderVisitor(void) override = default;

    concepts::stringable Visit(ModelStateLeaf &modelState) override;
    concepts::stringable Visit(ModelStateComposite &modelState) override;
    concepts::stringable Visit(ElementStatus &elementStatus) override;
};
#endif //KLONDIKE_PATTERNS_JSONGAMESTATEENCODERVISITOR_HPP
