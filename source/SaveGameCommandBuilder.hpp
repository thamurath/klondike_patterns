#ifndef KLONDIKE_PATTERNS_SAVEGAMECOMMANDBUILDER_HPP
#define KLONDIKE_PATTERNS_SAVEGAMECOMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class SaveGameCommandBuilder : public CommandBuilder
{
public:
    ~SaveGameCommandBuilder(void) override = default;

    std::unique_ptr<Command> Build(TableView &tableView) override;


    static std::string Description(void)
    {
        return "Save current Game";
    }
};

#endif //KLONDIKE_PATTERNS_SAVEGAMECOMMANDBUILDER_HPP
