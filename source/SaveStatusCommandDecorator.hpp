#ifndef KLONDIKE_PATTERNS_SAVESTATUSCOMMANDDECORATOR_HPP
#define KLONDIKE_PATTERNS_SAVESTATUSCOMMANDDECORATOR_HPP

#include "CommandBaseDecorator.hpp"

class SaveStatusCommandDecorator : public CommandBaseDecorator
{
public:
    using CommandBaseDecorator::CommandBaseDecorator;

    ~SaveStatusCommandDecorator() override = default;

    ReturnCode Execute(TableModel &modelInterface) override;

private:
    void saveCurrentStatus(TableModel &modelInterface) const;
};

#endif //KLONDIKE_PATTERNS_SAVESTATUSCOMMANDDECORATOR_HPP
