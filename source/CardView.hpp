#ifndef KLONDIKE_PATTERNS_CARDVIEW_HPP
#define KLONDIKE_PATTERNS_CARDVIEW_HPP

#include <string>

class CardView final
{
public:
    CardView(std::string figure, std::string suit, std::string color, std::string face);
    CardView(void) = default;
    ~CardView() = default;

    // allow copy
    CardView(CardView const& rhs) = default;
    CardView& operator=(CardView const& rhs) = default;
    // allow movement
    CardView(CardView&& rhs) = default;
    CardView& operator=(CardView&& rhs) = default;


    void Figure(std::string figure);
    void Suit(std::string suit);
    void Color(std::string color);
    void Face(std::string face);

    std::string const Figure(void) const;
    std::string const Suit(void) const;
    std::string const Color(void) const;
    std::string const Face(void) const;

private:
    std::string figure;
    std::string suit;
    std::string color;
    std::string face;
};

std::string to_string(CardView const& cardView);
#endif //KLONDIKE_PATTERNS_CARDVIEW_HPP
