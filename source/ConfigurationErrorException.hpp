#ifndef KLONDIKE_PATTERNS_CONFIGURATIONERROREXCEPTION_HPP
#define KLONDIKE_PATTERNS_CONFIGURATIONERROREXCEPTION_HPP

#include <exception>
#include <string>
#include <sstream>
#include <utility>

namespace exceptions
{
    class ConfigurationErrorException : public std::exception
    {
    public:
        explicit ConfigurationErrorException(std::string message)
                : message(std::move(message))
        {}

        ~ConfigurationErrorException() override = default;



        const char *what() const noexcept override
        {
            std::stringstream ss;
            ss << exception::what()
               << " : " << message;
            return ss.str().c_str();
        }
    private:
        std::string message;
    };
}

#endif //KLONDIKE_PATTERNS_CONFIGURATIONERROREXCEPTION_HPP
