
#ifndef KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMAND_HPP
#define KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMAND_HPP

#include "Command.hpp"

#include "FoundationView.hpp"
#include "PileView.hpp"

class PileToFoundationCommand : public Command
{
public:
    PileToFoundationCommand(PileView& pileView, FoundationView& foundationView);
    ~PileToFoundationCommand(void) override = default;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;

private:
    PileView& pileView;
    FoundationView& foundationView;
};



#endif //KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMAND_HPP
