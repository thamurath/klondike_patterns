#ifndef KLONDIKE_PATTERNS_GETNUMBERMENU_HPP
#define KLONDIKE_PATTERNS_GETNUMBERMENU_HPP

#include <cinttypes>
#include <string>

class AskForNumberMenu
{
public:
    AskForNumberMenu(std::int64_t min, std::int64_t max)
    : min(min), max(max)
    {

    }
    virtual ~AskForNumberMenu() = default;

    virtual std::int64_t Execute(std::string questionLabel) = 0;

protected:
    std::int64_t min;
    std::int64_t max;
};

#endif //KLONDIKE_PATTERNS_GETNUMBERMENU_HPP
