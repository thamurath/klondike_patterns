#ifndef KLONDIKE_PATTERNS_ENGLISHCOLOR_HPP
#define KLONDIKE_PATTERNS_ENGLISHCOLOR_HPP

#include "smart_enum.hpp"
#include "Color.hpp"

namespace english_card
{
    class EnglishColor : public smart_enum , public Color
    {
    public:
        using smart_enum::smart_enum;
        // Unique objects of this class
        static const EnglishColor RED;
        static const EnglishColor BLACK;
    };

    const EnglishColor EnglishColor::RED(0,"RED");
    const EnglishColor EnglishColor::BLACK(1,"BLACK");
}
#endif //KLONDIKE_PATTERNS_ENGLISHCOLOR_HPP
