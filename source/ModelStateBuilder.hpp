#ifndef KLONDIKE_PATTERNS_MODELSTATEBUILDER_HPP
#define KLONDIKE_PATTERNS_MODELSTATEBUILDER_HPP

#include "ModelState.hpp"
#include "ElementStatus.hpp"

#include <tuple>
#include <vector>
#include <memory>

class ElementView;


class ModelStateBuilder
{
public:
    using value_type = std::tuple<ElementView*, ElementStatus>;

    ModelStateBuilder(void) = default;
    ModelStateBuilder& addElement(value_type&& element);
    std::unique_ptr<ModelState> Build(void);
private:
    std::vector<value_type> elements;
};

#endif //KLONDIKE_PATTERNS_MODELSTATEBUILDER_HPP
