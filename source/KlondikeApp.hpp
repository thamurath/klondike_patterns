#ifndef KLONDIKE_KLONDIKEAPP_HPP
#define KLONDIKE_KLONDIKEAPP_HPP

#include "TableView.hpp"
#include "TableModel.hpp"
#include "ReturnCode.hpp"

class KlondikeApp final
{
public:
    KlondikeApp(void) = default;
    ~KlondikeApp(void) = default;
    
    //avoid copy
    KlondikeApp(KlondikeApp const& rhs) = delete;
    KlondikeApp& operator=(KlondikeApp const& rhs) = delete;
    
    //avoid movement
    KlondikeApp(KlondikeApp&& rhs) = delete;
    KlondikeApp& operator=(KlondikeApp&& rhs) = delete;

    ReturnCode Run(int argc, char** argv);
protected:
private:

    void InitializeUndoManager(TableModel &tableModel, TableView &tableView) const;
};



#endif //KLONDIKE_KLONDIKEAPP_HPP
