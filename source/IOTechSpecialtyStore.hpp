#ifndef KLONDIKE_PATTERNS_IOTECHSPECIALTYSTORE_HPP
#define KLONDIKE_PATTERNS_IOTECHSPECIALTYSTORE_HPP

#include "IOTechAbstractFactory.hpp"

#include <vector>
#include <memory>

class IOTechRegisterProxyBase;


class IOTechSpecialtyStore
{
public:
    static IOTechSpecialtyStore& Instance(void)
    {
        static IOTechSpecialtyStore instance;
        return instance;
    }

    void Register(IOTechRegisterProxyBase * const proxy);
    IOTechAbstractFactory& GetIOTech(void) const;

private:
    IOTechSpecialtyStore(void) = default;

    std::vector<const IOTechRegisterProxyBase*> registeredFactories;
    mutable std::unique_ptr<IOTechAbstractFactory> factory;
};

#endif //KLONDIKE_PATTERNS_IOTECHSPECIALTYSTORE_HPP
