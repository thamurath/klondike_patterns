#ifndef KLONDIKE_PATTERNS_UNDOMANAGER_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGER_HPP

#include "ModelState.hpp"
#include "UndoManagerContext.hpp"

#include <memory>

// Nota para que esto funcione siempre tiene que estar cargado el estado mas actual.
// Es decir que siempre que el sistema cambie de estado tenemos que cargarlo.
namespace undo_manager
{
    class UndoManager final
    {
    public:
        static UndoManager& Instance(void)
        {
            static UndoManager instance;
            return instance;
        }

        ~UndoManager() = default;

        // avoid copy
        UndoManager(UndoManager const& rhs) = delete;
        UndoManager& operator=(UndoManager const& rhs) = delete;

        //avoid movement
        UndoManager(UndoManager&& rhs) noexcept = delete;
        UndoManager& operator=(UndoManager&& rhs) noexcept = delete;

        void to_do(std::unique_ptr<ModelState> modelState);
        ModelState const& to_undo(void);
        ModelState const& to_redo(void);

    private:
        UndoManager(void);
        void SetInitialState(std::unique_ptr<ModelState> modelState);
        Context context;
        bool initialized;

        friend class UndoManagerBuilder;
    };

    class UndoManagerBuilder
    {
    public:
        void CreateUndoManager(std::unique_ptr<ModelState> initialState)
        {
            UndoManager::Instance().SetInitialState(std::move(initialState));
        }
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGER_HPP
