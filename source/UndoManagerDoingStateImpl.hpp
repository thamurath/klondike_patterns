#ifndef KLONDIKE_PATTERNS_UNDOMANAGERDOINGSTATEIMPL_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERDOINGSTATEIMPL_HPP

#include "UndoManagerStateImpl.hpp"

namespace undo_manager
{
    class DoingStateImpl : public StateImpl
    {
    public:
        explicit DoingStateImpl(Context& context);
        ~DoingStateImpl() override = default;

        // avoid copy
        DoingStateImpl(DoingStateImpl const& rhs) = delete;
        DoingStateImpl& operator=(DoingStateImpl const& rhs) = delete;

        // avoid movement
        DoingStateImpl(DoingStateImpl&& rhs) noexcept = delete;
        DoingStateImpl& operator=(DoingStateImpl&& rhs) noexcept = delete;

        void to_do(std::unique_ptr<ModelState> modelState) override ;
        const ModelState & to_undo(void) override ;
        const ModelState & to_redo(void) override ;

        void updateCurrentState(void) const override ;
    private:
        mutable bool shouldChange;
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGERDOINGSTATEIMPL_HPP
