#ifndef KLONDIKE_PATTERNS_PILETOPILECOMMAND_HPP
#define KLONDIKE_PATTERNS_PILETOPILECOMMAND_HPP

#include "Command.hpp"

#include "PileView.hpp"

class PileToPileCommand : public Command
{
public:
    PileToPileCommand(PileView& pileViewFrom, PileView& pileViewTo, std::uint8_t numCards);
    ~PileToPileCommand(void) override = default;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;

private:
    PileView& pileViewFrom;
    PileView& pileViewTo;
    std::uint8_t numCards;
};

#endif //KLONDIKE_PATTERNS_PILETOPILECOMMAND_HPP
