
#ifndef KLONDIKE_PATTERNS_FIGURE_HPP
#define KLONDIKE_PATTERNS_FIGURE_HPP

class Figure
{
public:
    virtual ~Figure() = 0;
};

#endif //KLONDIKE_PATTERNS_FIGURE_HPP
