#ifndef KLONDIKE_TABLEVIEW_HPP
#define KLONDIKE_TABLEVIEW_HPP

#include "ElementViewIterator.hpp"
#include "ElementView.hpp"
#include "ElementType.hpp"


#include "data_types.hpp"

#include <optional.hpp>

#include <string>
#include <map>
#include <cassert>
#include <ReturnCode.hpp>

class CommandBuilderManager;
class ControlInterface;

class TableView final
{
public:
    TableView(CommandBuilderManager& builderManager, ControlInterface& controlInterface);
    ~TableView(void) = default;
    
    //avoid copy
    TableView(TableView const& rhs) = delete;
    TableView& operator=(TableView const& rhs) = delete;
    
    //avoid movement
    TableView(TableView&& rhs) = delete;
    TableView& operator=(TableView&& rhs) = delete;

    ReturnCode Run(void);

    ElementViewCollectionNotOwning GetAllElements(void);
    ElementViewCollectionNotOwning GetAllElements(ElementType const &type);
    ElementView& GetElement(std::string const& id);
protected:
private:
    CommandBuilderManager& builderManager;
    ControlInterface& controlInterface;

    ElementViewCollection views;
    std::map<std::string,ElementView*> by_id;
    std::multimap<ElementType, ElementView*> by_type;


    bool notExit(ReturnCode const& returnCode) const;
};



#endif //KLONDIKE_TABLEVIEW_HPP
