#ifndef KLONDIKE_PATTERNS_IOTECHTERMINALFACTORY_HPP
#define KLONDIKE_PATTERNS_IOTECHTERMINALFACTORY_HPP


#include "IOTechAbstractFactory.hpp"

class IOTechTerminalFactory final : public IOTechAbstractFactory
{
public:
    IOTechTerminalFactory(void) = default;
    ~IOTechTerminalFactory(void) override = default;
    std::unique_ptr<NumberedOptionMenu> GetNumberedOptionMenu(std::vector<concepts::stringable>&& options) override;
    std::unique_ptr<AskForNumberMenu> GetAskForNumberMenu(std::int64_t min, std::int64_t max) override;
    std::unique_ptr<AskForStringMenu> GetAskForStringMenu(std::uint32_t min_size = 0) override ;
    // Criteria methods
    static std::string GetAbstractFactoryType(void);

};

#endif //KLONDIKE_PATTERNS_IOTECHTERMINALFACTORY_HPP
