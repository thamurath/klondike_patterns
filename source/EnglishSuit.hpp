#ifndef KLONDIKE_PATTERNS_ENGLISHSUIT_HPP
#define KLONDIKE_PATTERNS_ENGLISHSUIT_HPP

#include "smart_enum.hpp"
#include "Suit.hpp"


namespace english_card
{
    class EnglishSuit final : public smart_enum, public Suit
    {
    public:
        using smart_enum::smart_enum;
        // unique objects for this type
        static const EnglishSuit SPADES;
        static const EnglishSuit CLUBS;
        static const EnglishSuit HEARTS;
        static const EnglishSuit DIAMONDS;
    };

    const EnglishSuit EnglishSuit::SPADES(2, "SPADES");
    const EnglishSuit EnglishSuit::CLUBS(4, "CLUBS");
    const EnglishSuit EnglishSuit::HEARTS(1, "HEARTS");
    const EnglishSuit EnglishSuit::DIAMONDS(3, "DIAMONDS");
}
#endif //KLONDIKE_PATTERNS_ENGLISHSUIT_HPP
