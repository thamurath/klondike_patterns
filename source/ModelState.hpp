#ifndef KLONDIKE_PATTERNS_MODELSTATE_HPP
#define KLONDIKE_PATTERNS_MODELSTATE_HPP

#include "TableModel.hpp"
#include "GameStateEncoderVisitable.hpp"

class ModelState : public GameStateEncoderVisitable
{
public:
    ModelState() = default;
    virtual ~ModelState(void) = default;

    virtual void load(TableModel& modelInterface) = 0;

    //allow movement
    ModelState(ModelState&& rhs) noexcept = default;
    ModelState& operator=(ModelState&& rhs) noexcept = default;
    //avoid copy
    ModelState(ModelState const& rhs) = delete;
    ModelState& operator=(ModelState const& rhs) = delete;

protected:

private:

};

#endif //KLONDIKE_PATTERNS_MODELSTATE_HPP
