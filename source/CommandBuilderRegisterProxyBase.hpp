#ifndef KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXYBASE_HPP
#define KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXYBASE_HPP

#include "CommandBuilder.hpp"

#include <memory>

class CommandBuilderRegisterProxyBase
{
public:
    CommandBuilderRegisterProxyBase(void);
    virtual ~CommandBuilderRegisterProxyBase(void) = default;

    virtual std::unique_ptr<CommandBuilder> CreateCommandBuilder(void) const = 0;
    friend std::string to_string(CommandBuilderRegisterProxyBase const* self)
    {
        return self->str();
    }

private:
    // To allow Proxies to be "stringables"
    virtual std::string str(void) const = 0;

};
#endif //KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXYBASE_HPP
