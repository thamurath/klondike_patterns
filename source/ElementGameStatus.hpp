#ifndef KLONDIKE_PATTERNS_ELEMENTGAMESTATUS_HPP
#define KLONDIKE_PATTERNS_ELEMENTGAMESTATUS_HPP

#include "CardView.hpp"

#include <vector>
#include <utility>
#include <string>
#include <sstream>

class ElementGameStatus final
{
public:
    ElementGameStatus(std::vector<CardView> cards)
            : cards(std::move(cards))
    {

    }
    ElementGameStatus(void) = default;
    ~ElementGameStatus() = default;



    void SetCards(std::vector<CardView> cards)
    {
        this->cards = std::move(cards);
    }

    std::vector<CardView> GetCards(void) const
    {
        return cards;
    }

public:
    std::vector<CardView> cards;
};

std::string to_string(ElementGameStatus const& status)
{
    std::stringstream ss;
    ss << "[";
    auto cards = status.GetCards();
    ss << "{" << to_string(cards.at(0));
    for (auto idx = 1; idx < std::size(cards); ++idx)
    {
        ss << "},{" << to_string(cards.at(idx));
    }
    ss << "}]";
    return ss.str();
}
#endif //KLONDIKE_PATTERNS_ELEMENTGAMESTATUS_HPP
