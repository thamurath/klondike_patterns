#ifndef KLONDIKE_PATTERNS_PILEVIEW_HPP
#define KLONDIKE_PATTERNS_PILEVIEW_HPP


#include "ElementView.hpp"

class PileView final : public ElementView
{
public:
    using ElementView::ElementView;

    ~PileView(void) override = default;

    //allow movement
    PileView(PileView&& rhs) noexcept = default;
    PileView& operator=(PileView&& rhs) noexcept = default;

    void Decode(ViewStatus const &status) override;
    void ShowStatus(void) override;
    PileView* Clone(void) const override;

protected:
private:
    //allow only private copy
    PileView(PileView const& rhs) = default;
    PileView& operator=(PileView const& rhs) = default;
};


#endif //KLONDIKE_PATTERNS_PILEVIEW_HPP
