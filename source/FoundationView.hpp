#ifndef KLONDIKE_PATTERNS_FOUNDATIONVIEW_HPP
#define KLONDIKE_PATTERNS_FOUNDATIONVIEW_HPP

#include "ElementView.hpp"

class FoundationView final : public ElementView
{
public:
    using ElementView::ElementView;

    ~FoundationView(void) override = default;

    //allow movement
    FoundationView(FoundationView&& rhs) noexcept = default;
    FoundationView& operator=(FoundationView&& rhs) noexcept = default;

    void Decode(ViewStatus const &status) override;
    void ShowStatus(void) override;
    FoundationView* Clone(void) const override;

protected:
private:
    //allow only private copy
    FoundationView(FoundationView const& rhs) = default;
    FoundationView& operator=(FoundationView const& rhs) = default;
};


#endif //KLONDIKE_PATTERNS_FOUNDATIONVIEW_HPP
