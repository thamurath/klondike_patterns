#ifndef KLONDIKE_PATTERNS_UNDOMANAGERSTATEDISPATCHER_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERSTATEDISPATCHER_HPP

#include "UndoManagerStateImpl.hpp"

namespace undo_manager
{
    class StateDispatcher final
    {
    public:
        StateDispatcher(StateImpl& stateImpl);
        ~StateDispatcher();

        StateImpl* operator->();
    private:
        StateImpl& stateImpl;
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGERSTATEDISPATCHER_HPP
