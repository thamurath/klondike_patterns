#ifndef KLONDIKE_PATTERNS_PILEMODEL_HPP
#define KLONDIKE_PATTERNS_PILEMODEL_HPP

#include "ElementModel.hpp"

class PileModel : public ElementModel
{
public:
    using ElementModel::ElementModel;
    ~PileModel() override  = default;

    ElementStatus SaveStatus(void) const override;
    void LoadStatus(ElementStatus const &status) override;

    void AddCard(Card &&card) override;
    void ExtractCard(void) override;
    std::optional<Card> GetCard(void) const override ;
    std::vector<Card> GetCards(std::size_t numCards) const override ;

};
#endif //KLONDIKE_PATTERNS_PILEMODEL_HPP
