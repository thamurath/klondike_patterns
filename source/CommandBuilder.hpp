#ifndef KLONDIKE_COMMANDBUILDER_HPP
#define KLONDIKE_COMMANDBUILDER_HPP

#include "TableView.hpp"
#include "Command.hpp"

#include <string>
#include <memory>

class CommandBuilder
{
public:
    CommandBuilder(void) = default;
    virtual ~CommandBuilder(void) = default;

    virtual std::unique_ptr<Command> Build(TableView &tableView) = 0;
};


#endif //KLONDIKE_COMMANDBUILDER_HPP
