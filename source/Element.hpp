#ifndef KLONDIKE_PATTERNS_ELEMENT_HPP
#define KLONDIKE_PATTERNS_ELEMENT_HPP

#include "ElementType.hpp"
#include <string>


class Element
{
public:
    Element(std::string id, ElementType const& type)
            : id(id)
            , type(type)
    {}

    virtual ~Element(void) = default;

protected:
    std::string const& getId(void) const
    {
        return id;
    }
    ElementType const& getType(void) const
    {
        return type;
    }
private:
    std::string id;
    ElementType type;
};

#endif //KLONDIKE_PATTERNS_ELEMENT_HPP
