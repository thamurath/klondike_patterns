#ifndef KLONDIKE_PATTERNS_INFORMATIONHUB_HPP
#define KLONDIKE_PATTERNS_INFORMATIONHUB_HPP

#include "NotificationBase.hpp"
#include "ObserverAdapter.hpp"

#include <Poco/NotificationCenter.h>
#include <memory>

class InformationHub final
{
public:
    InformationHub(void) = default;
    ~InformationHub() = default;


    template<typename T, typename C>
    void AddObserver(ObserverAdapter<T,C> const& observer)
    {
        notificationCenter.addObserver(observer);
    }


    void PostNotification(NotificationBase* notificationPtr)
    {
        std::unique_ptr<NotificationBase> p(notificationPtr);
        notificationCenter.postNotification(notificationPtr);
    }

    // avoid copy
    InformationHub(InformationHub const& ) = delete;
    InformationHub& operator=(InformationHub const& ) = delete;

    // avoid move
    InformationHub(InformationHub&& ) = delete;
    InformationHub& operator=(InformationHub&& ) = delete;

protected:
private:
    Poco::NotificationCenter notificationCenter;
};

#endif //KLONDIKE_PATTERNS_INFORMATIONHUB_HPP
