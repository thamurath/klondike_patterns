#ifndef KLONDIKE_PATTERNS_IOTECHREGISTERPROXYBASE_HPP
#define KLONDIKE_PATTERNS_IOTECHREGISTERPROXYBASE_HPP

#include <memory>

class IOTechAbstractFactory;

class IOTechRegisterProxyBase
{
public:
    IOTechRegisterProxyBase(void);
    virtual  ~IOTechRegisterProxyBase() = default;

    // For the store to be able to create elements
    virtual std::unique_ptr<IOTechAbstractFactory> CreateObject(void) const = 0;

    // Choose criteria for Store to create the correct object
    virtual std::string GetAbstractFactoryType(void) const = 0;
};

#endif //KLONDIKE_PATTERNS_IOTECHREGISTERPROXYBASE_HPP
