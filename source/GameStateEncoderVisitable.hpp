#ifndef KLONDIKE_PATTERNS_GAMESTATEENCODERVISITABLE_HPP
#define KLONDIKE_PATTERNS_GAMESTATEENCODERVISITABLE_HPP

#include "GameStateEncoderVisitor.hpp"

#include "stringable.hpp"

class GameStateEncoderVisitable
{
public:
    virtual ~GameStateEncoderVisitable(void) = default;

    virtual concepts::stringable Accept(GameStateEncoderVisitor& visitor) = 0;
};

#endif //KLONDIKE_PATTERNS_GAMESTATEENCODERVISITABLE_HPP
