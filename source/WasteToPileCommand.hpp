#ifndef KLONDIKE_PATTERNS_WASTETOPILECOMMAND_HPP
#define KLONDIKE_PATTERNS_WASTETOPILECOMMAND_HPP

#include "Command.hpp"

#include "PileView.hpp"
#include "WasteView.hpp"

class WasteToPileCommand : public Command
{
public:
    WasteToPileCommand(WasteView& wasteView, PileView& pileView);
    ~WasteToPileCommand(void) override = default;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;

private:
    WasteView& wasteView;
    PileView& pileView;
};


#endif //KLONDIKE_PATTERNS_WASTETOPILECOMMAND_HPP
