#ifndef KLONDIKE_CARDFACTORY_HPP
#define KLONDIKE_CARDFACTORY_HPP

#include "Card.hpp"

#include <vector>
#include <cassert>
///@jlom TODO Aun no tengo hecho todo lo relacioniado con Card, Suit, Figure y Color para que pueda haber distintos
/// tipos de cartas

class CardFactory
{
public:

    static CardFactory& Instance(void)
    {
        static CardFactory instance;
        return instance;
    }

    std::vector<Card> CreateDeck(void)
    {
        assert(false);
        return {};
    }

    virtual ~CardFactory(void) {};
    
    //avoid copy
    CardFactory(CardFactory const& rhs) = delete;
    CardFactory& operator=(CardFactory const& rhs) = delete;
protected:
private:
    CardFactory(void) = default;
};



#endif //KLONDIKE_CARDFACTORY_HPP
