#ifndef KLONDIKE_PATTERNS_TERMINALASKFORSTRINGMENU_HPP
#define KLONDIKE_PATTERNS_TERMINALASKFORSTRINGMENU_HPP

#include "AskForStringMenu.hpp"

class TerminalAskForStringMenu final : public AskForStringMenu
{
public:
    using AskForStringMenu::AskForStringMenu;
    ~TerminalAskForStringMenu() override  = default;

    std::string Execute(std::string questionLabel) override;

};
#endif //KLONDIKE_PATTERNS_TERMINALASKFORSTRINGMENU_HPP
