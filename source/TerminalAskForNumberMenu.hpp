#ifndef KLONDIKE_PATTERNS_TERMINALASKFORNUMBERMENU_HPP
#define KLONDIKE_PATTERNS_TERMINALASKFORNUMBERMENU_HPP

#include "AskForNumberMenu.hpp"

#include <utility>

class TerminalAskForNumberMenu final : public AskForNumberMenu
{
public:
    using AskForNumberMenu::AskForNumberMenu;
    ~TerminalAskForNumberMenu() override  = default;

    std::int64_t Execute(std::string questionLabel) override;

};
#endif //KLONDIKE_PATTERNS_TERMINALASKFORNUMBERMENU_HPP
