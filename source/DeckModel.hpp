
#ifndef KLONDIKE_PATTERNS_DECKMODEL_HPP
#define KLONDIKE_PATTERNS_DECKMODEL_HPP

#include <optional.hpp>
#include "ElementModel.hpp"


class DeckModel final : public ElementModel
{
public:
    DeckModel(std::string&& id, ElementType&& type, std::vector<Card>&& cards);
    ~DeckModel() override  = default;

    ElementStatus SaveStatus(void) const override;
    void LoadStatus(ElementStatus const &status) override;

    void AddCard(Card &&card) override;
    void ExtractCard(void) override;
    std::optional<Card> GetCard(void) const override ;
    std::vector<Card> GetCards(std::size_t numCards) const override ;
    std::vector<Card> GetAllCards(void) const override ;

};

#endif //KLONDIKE_PATTERNS_DECKMODEL_HPP
