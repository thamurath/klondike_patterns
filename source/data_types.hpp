#ifndef KLONDIKE_DATA_TYPES_HPP
#define KLONDIKE_DATA_TYPES_HPP

#include "ElementModel.hpp"
#include "ElementViewCollection.hpp"


#include <vector>
#include <memory>



using ElementCollection = std::vector<std::unique_ptr<ElementModel>>;

#endif //KLONDIKE_DATA_TYPES_HPP
