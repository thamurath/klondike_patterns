#ifndef KLONDIKE_PATTERNS_WASTETOPILECOMMANDBUILDER_HPP
#define KLONDIKE_PATTERNS_WASTETOPILECOMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class WasteToPileCommandBuilder : public CommandBuilder
{
public:
    ~WasteToPileCommandBuilder(void) override = default;

    std::unique_ptr<Command> Build(TableView &tableView) override;


    static std::string Description(void)
    {
        return "Waste to Pile";
    }
};

#endif //KLONDIKE_PATTERNS_WASTETOPILECOMMANDBUILDER_HPP
