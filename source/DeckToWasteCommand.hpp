#ifndef KLONDIKE_DECKTOWASTE_COMMAND_HPP
#define KLONDIKE_DECKTOWASTE_COMMAND_HPP

#include "Command.hpp"

#include "DeckView.hpp"
#include "WasteView.hpp"

class DeckToWasteCommand final : public Command
{
public:
    DeckToWasteCommand(DeckView* deckView, WasteView* wasteView);

    ~DeckToWasteCommand(void) override = default;

    //avoid copy
    DeckToWasteCommand(DeckToWasteCommand const& rhs) = delete;
    DeckToWasteCommand& operator=(DeckToWasteCommand const& rhs) = delete;
    
    //avoid movement
    DeckToWasteCommand(DeckToWasteCommand&& rhs) = delete;
    DeckToWasteCommand& operator=(DeckToWasteCommand&& rhs) = delete;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;
private:
    DeckView* deckView;
    WasteView* wasteView;
};



#endif //KLONDIKE_DECKTOWASTE_COMMAND_HPP
