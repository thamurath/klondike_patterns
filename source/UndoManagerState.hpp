#ifndef KLONDIKE_PATTERNS_UNDOMANAGERSTATE_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERSTATE_HPP

#include "UndoManagerStateDispatcher.hpp"
#include "UndoManagerStateImpl.hpp"

#include <memory>

namespace undo_manager
{
    class State
    {
    public:
        State(std::unique_ptr<StateImpl>&& impl);
        StateDispatcher operator->();

        void Reset(std::unique_ptr<StateImpl>&& newImpl);

    private:
        std::unique_ptr<StateImpl> impl;
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGERSTATE_HPP
