#ifndef KLONDIKE_COMMANDBUILDERMANAGER_HPP
#define KLONDIKE_COMMANDBUILDERMANAGER_HPP

#include "CommandBuilderContainer.hpp"

#include <vector>



class CommandBuilderManager final 
{
public:
    CommandBuilderManager(void) = default;
    ~CommandBuilderManager(void) = default;
    
    //avoid copy
    CommandBuilderManager(CommandBuilderManager const& rhs) = delete;
    CommandBuilderManager& operator=(CommandBuilderManager const& rhs) = delete;
    
    //avoid movement
    CommandBuilderManager(CommandBuilderManager&& rhs) = delete;
    CommandBuilderManager& operator=(CommandBuilderManager&& rhs) = delete;

    CommandBuilderContainer GetAvailableCommands(void);

protected:
private:
};



#endif //KLONDIKE_COMMANDBUILDERMANAGER_HPP
