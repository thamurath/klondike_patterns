#ifndef KLONDIKE_TABLEMODEL_HPP
#define KLONDIKE_TABLEMODEL_HPP

#include "ReturnCode.hpp"
#include "data_types.hpp"

#include "ElementStatus.hpp"
#include "ElementView.hpp"

#include <optional.hpp>

#include <map>
#include <functional>
class TableModel final
{
public:
    TableModel(void);
    ~TableModel(void) = default;

    //avoid copy
    TableModel(TableModel const &rhs) = delete;
    TableModel &operator=(TableModel const &rhs) = delete;

    //avoid movement
    TableModel(TableModel &&rhs) = delete;
    TableModel &operator=(TableModel &&rhs) = delete;


    std::optional<Card> GetCard(ElementView& elementView) const;
    std::vector<Card> GetCards(ElementView& elementView, size_t numCards) const;
    std::size_t GetNumCards(ElementView& elementView) const;
    bool IsEmpty(ElementView& elementView) const;
    std::vector<Card> GetAllCards(ElementView& elementView) const;

    void loadElementState(ElementView& elementView, ElementStatus& status);
    std::optional<ElementStatus> saveElementState(ElementView& elementView);
    ReturnCode move(ElementView& from, ElementView& to, const std::uint8_t& num_cards,std::function<void(Card&)> modifyFunctor = [](Card&){});

protected:
private:
    ElementCollection models;
    std::map<std::string,ElementModel*> by_id;
    std::multimap<ElementType, ElementModel*> by_type;

    ElementModel* findById(std::string id) const;
};




#endif //KLONDIKE_TABLEMODEL_HPP
