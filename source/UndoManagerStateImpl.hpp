#ifndef KLONDIKE_PATTERNS_UNDOMANAGERSTATEIMPL_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERSTATEIMPL_HPP

#include "ModelState.hpp"


#include <memory>

namespace undo_manager
{
    class Context;

    class StateImpl
    {
    public:
        StateImpl(Context& context);
        virtual ~StateImpl() = default;

        // avoid copy
        StateImpl(StateImpl const& rhs) = delete;
        StateImpl& operator=(StateImpl const& rhs) = delete;

        // avoid movement
        StateImpl(StateImpl&& rhs) noexcept = delete;
        StateImpl& operator=(StateImpl&& rhs) noexcept = delete;

        virtual void to_do(std::unique_ptr<ModelState> modelState) = 0;
        virtual ModelState const& to_undo(void) = 0;
        virtual ModelState const& to_redo(void) = 0;

        virtual void updateCurrentState(void) const = 0;

    protected:
        Context& getContext(void) const;
    private:
        Context& context;
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGERSTATEIMPL_HPP
