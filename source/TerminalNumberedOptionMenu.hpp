#ifndef KLONDIKE_PATTERNS_TERMINALNUMBEREDOPTIONMENU_HPP
#define KLONDIKE_PATTERNS_TERMINALNUMBEREDOPTIONMENU_HPP

#include "NumberedOptionMenu.hpp"

#include <utility>

class TerminalNumberedOptionMenu final : public NumberedOptionMenu
{
public:
    using NumberedOptionMenu::NumberedOptionMenu;
    ~TerminalNumberedOptionMenu() override  = default;

    std::uint32_t Execute(std::string questionLabel) override;
private:
    std::vector<concepts::stringable> options;
};
#endif //KLONDIKE_PATTERNS_TERMINALNUMBEREDOPTIONMENU_HPP
