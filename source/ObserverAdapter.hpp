#ifndef KLONDIKE_OBSERVERADAPTER_HPP
#define KLONDIKE_OBSERVERADAPTER_HPP

#include <Poco/Observer.h>
#include <Poco/AutoPtr.h>

#include <memory>
#include <iostream>
namespace com::jlom::klondike
{
    template <typename Target, typename Notification>
    class ObserverAdapter
    {
    public:
        typedef void (Target::*Callback)(Notification*);
        ObserverAdapter(Target& target, Callback methodToCall)
                : observer(target,methodToCall)
        {

        }

        void notify(Notification* notification) const
        {
            observer.notify(notification);
        }

        operator Poco::Observer<Target,Notification>(void) const
        {
            return observer;
        };

        operator Poco::Observer<Target,Notification> const& (void) const
        {
            return observer;
        };


    private:
        Poco::Observer<Target, Notification> observer;
    };
}

#endif //KLONDIKE_OBSERVERADAPTER_HPP
