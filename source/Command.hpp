#ifndef KLONDIKE_COMMAND_HPP
#define KLONDIKE_COMMAND_HPP

#include "ReturnCode.hpp"

#include <vector>

class TableModel;
class ElementView;

class Command
{
public:

    virtual ReturnCode Check(TableModel &modelInterface) = 0;
    virtual ReturnCode Execute(TableModel &modelInterface) = 0;
    virtual std::vector<ElementView*> GetInvolvedElements(void) const = 0;

    virtual ~Command(void) = default;

private:
};

#endif //KLONDIKE_COMMAND_HPP
