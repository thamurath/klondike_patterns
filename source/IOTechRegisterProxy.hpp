#ifndef KLONDIKE_PATTERNS_IOTECHREGISTERPROXY_HPP
#define KLONDIKE_PATTERNS_IOTECHREGISTERPROXY_HPP

#include "IOTechRegisterProxyBase.hpp"

template <typename IOTechConcreteFactory>
class IOTechRegisterProxy : public IOTechRegisterProxyBase
{
public:
    std::unique_ptr<IOTechAbstractFactory> CreateObject(void) const override
    {
        return std::make_unique<IOTechConcreteFactory>();
    }

    // Criterio de eleccion
    std::string GetAbstractFactoryType(void) const override
    {
        //relay on static method 
        return IOTechConcreteFactory::GetAbstractFactoryType();
    }
};

#endif //KLONDIKE_PATTERNS_IOTECHREGISTERPROXY_HPP
