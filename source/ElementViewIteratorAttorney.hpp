#ifndef KLONDIKE_ELEMENTVIEWITERATOR_ATTORNEY_HPP
#define KLONDIKE_ELEMENTVIEWITERATOR_ATTORNEY_HPP

#include "ElementViewIterator.hpp"

class ElementViewIteratorAttorney final
{
public:
    //avoid copy
    ElementViewIteratorAttorney(ElementViewIteratorAttorney const& rhs) = delete;
    ElementViewIteratorAttorney& operator=(ElementViewIteratorAttorney const& rhs) = delete;

    //avoid movement
    ElementViewIteratorAttorney(ElementViewIteratorAttorney&& rhs) = delete;
    ElementViewIteratorAttorney& operator=(ElementViewIteratorAttorney&& rhs) = delete;

private:

    static ElementViewIterator GetIterator(void)
    {
        return ElementViewIterator{};
    }

    ElementViewIteratorAttorney(void) = default;
    ~ElementViewIteratorAttorney(void) = default;
    

protected:
private:

    friend class TableView;
};



#endif //KLONDIKE_ELEMENTVIEWITERATOR_ATTORNEY_HPP
