
#ifndef KLONDIKE_PATTERNS_COMMANDBASEDECORATOR_HPP
#define KLONDIKE_PATTERNS_COMMANDBASEDECORATOR_HPP

#include "Command.hpp"

#include <memory>
#include <utility>

class CommandBaseDecorator : public Command
{
public:
    explicit CommandBaseDecorator(std::unique_ptr<Command> cmd)
            : cmd(std::move(cmd))
    {}
    ~CommandBaseDecorator() override  = default;

    ReturnCode Check(TableModel &modelInterface) override
    {
        return cmd->Check(std::forward<decltype(modelInterface)>(modelInterface));
    }
    ReturnCode Execute(TableModel &modelInterface) override
    {
        return cmd->Execute(std::forward<decltype(modelInterface)>(modelInterface));
    }
    std::vector<ElementView*> GetInvolvedElements(void) const override
    {
        return cmd->GetInvolvedElements();
    }


protected:
    std::unique_ptr<Command> cmd;
};
#endif //KLONDIKE_PATTERNS_COMMANDBASEDECORATOR_HPP
