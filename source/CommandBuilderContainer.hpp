
#ifndef KLONDIKE_PATTERNS_COMMANDBUILDERCONTAINER_HPP
#define KLONDIKE_PATTERNS_COMMANDBUILDERCONTAINER_HPP

#include "stringable.hpp"
#include "CommandBuilderRegisterProxyBase.hpp"
//#include "CommandBuilder.hpp"

#include <vector>
#include <iterator>

class CommandBuilder;

class CommandBuilderContainer final
{
public:
    using value_type = CommandBuilderRegisterProxyBase const *;
    using index_type = std::uint32_t ;

    CommandBuilderContainer(std::vector<value_type>&& elements)
            : elements(std::move(elements))
    {

    }
    ~CommandBuilderContainer() = default;

    operator std::vector<concepts::stringable>()
    {
        std::vector<concepts::stringable> ret (std::begin(elements), std::end(elements));
        return ret;
    }


    value_type& operator[](index_type idx)
    {
        return elements.at(idx);
    }

    auto begin(void) noexcept
    {
        return std::begin(elements);
    }
    auto end(void) noexcept
    {
        return std::end(elements);
    }

    auto begin(void) const noexcept
    {
        return std::begin(elements);
    }
    auto end(void) const noexcept
    {
        return std::end(elements);
    }

//    value_type& operator[](index_type idx) const
//    {
//        return elements.at(idx);
//    }
private:
    std::vector<value_type> elements;
};

#endif //KLONDIKE_PATTERNS_COMMANDBUILDERCONTAINER_HPP
