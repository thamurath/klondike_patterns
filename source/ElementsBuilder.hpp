#ifndef KLONDIKE_ELEMENTSBUILDER_HPP
#define KLONDIKE_ELEMENTSBUILDER_HPP

#include "ElementType.hpp"
#include "data_types.hpp"

class ElementsBuilder final
{
public:
    static ElementsBuilder& Instance(void);
    ~ElementsBuilder() = default;

    ElementsBuilder(ElementsBuilder const& rhs) = delete;
    ElementsBuilder operator=(ElementsBuilder const& rhs) = delete;

    ElementsBuilder(ElementsBuilder&& rhs) noexcept = delete;
    ElementsBuilder operator=(ElementsBuilder&& rhs) noexcept = delete;

    ElementViewCollection GetElementViews(void);
    ElementCollection GetElements(void);

protected:
private:
    ElementsBuilder() = default;
};

#endif //KLONDIKE_ELEMENTSBUILDER_HPP
