#ifndef KLONDIKE_CONFIGURATION_HPP
#define KLONDIKE_CONFIGURATION_HPP

class Configuration
{
public:
    virtual ~Configuration() =0 {};
protected:
private:
};

#endif //KLONDIKE_CONFIGURATION_HPP
