#ifndef KLONDIKE_ELEMENT_HPP
#define KLONDIKE_ELEMENT_HPP

#include "Element.hpp"
#include "ElementType.hpp"

#include "ElementStatus.hpp"
#include "Card.hpp"

#include <string>
#include <vector>
#include <optional.hpp>

class ElementModel : public Element
{
public:
    using Element::Element;
    ElementModel(std::string&& id, ElementType&& type, std::vector<Card>&& cards);

    virtual ~ElementModel(void) = default;

    //avoid copy
    ElementModel(ElementModel const &rhs) = delete;
    ElementModel &operator=(ElementModel const &rhs) = delete;

    //avoid movement
    ElementModel(ElementModel &&rhs) = delete;
    ElementModel &operator=(ElementModel &&rhs) = delete;

    using Element::getType;
    using Element::getId;


    virtual ElementStatus SaveStatus(void) const = 0;
    virtual void LoadStatus(ElementStatus const& status) = 0;




    virtual void AddCard(Card&& card);
    virtual void ExtractCard(void);
    virtual std::optional<Card> GetCard(void) const;
    virtual std::vector<Card> GetCards(size_t numCards) const;
    virtual std::size_t GetNumCards(void) const;
    virtual bool IsEmpty(void) const;
    virtual std::vector<Card> GetAllCards(void) const;

protected:
    std::vector<Card> cards;

};


#endif //KLONDIKE_ELEMENT_HPP
