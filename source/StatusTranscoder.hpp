
#ifndef KLONDIKE_PATTERNS_STATUSTRANSCODER_HPP
#define KLONDIKE_PATTERNS_STATUSTRANSCODER_HPP

#include "ViewStatus.hpp"
#include "Card.hpp"

#include <vector>
#include <memory>

class StatusTranscoder
{
public:

    virtual ~StatusTranscoder() = default;

    virtual std::vector<Card> Decode(ViewStatus& status) = 0;
    virtual std::unique_ptr<ViewStatus> Encode(std::vector<Card> cards) = 0;
};

#endif //KLONDIKE_PATTERNS_STATUSTRANSCODER_HPP
