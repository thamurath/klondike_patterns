#ifndef KLONDIKE_PATTERNS_WASTEVIEW_HPP
#define KLONDIKE_PATTERNS_WASTEVIEW_HPP

#include "ElementView.hpp"

class WasteView final : public ElementView
{
public:
    using ElementView::ElementView;

    ~WasteView(void) override = default;

    //allow movement
    WasteView(WasteView&& rhs) noexcept = default;
    WasteView& operator=(WasteView&& rhs) noexcept = default;

    void Decode(ViewStatus const &status) override;
    void ShowStatus(void) override;
    WasteView* Clone(void) const override;

protected:
private:
    //allow only private copy
    WasteView(WasteView const& rhs) = default;
    WasteView& operator=(WasteView const& rhs) = default;
};


#endif //KLONDIKE_PATTERNS_WASTEVIEW_HPP
