#ifndef KLONDIKE_PATTERNS_STRINGABLE_HPP
#define KLONDIKE_PATTERNS_STRINGABLE_HPP

#include <string>
#include <type_traits>
#include <utility>
#include <memory>

namespace concepts
{
    // stringable type is anyone the compiler could find a "to_string" funcion.
    ///@note: Use notstd::to_string to enable the adl resolution
    ///@see: to_string.hpp
    class stringable
    {
        struct stringable_concept
        {
            virtual ~stringable_concept() = default;
            virtual std::string do_to_string(void) const = 0;
            /// Addendum : to enable polymorphic copy
            virtual std::unique_ptr<stringable_concept> clone(void) const = 0;
        };

        template <typename Implementor>
        struct stringable_model : public stringable_concept
        {
            stringable_model(void) = default;

            stringable_model(Implementor const& impl)
            : implementor(impl)
            {}

            stringable_model(Implementor&& impl)
                    : implementor(std::move(impl))
            {}

            std::string do_to_string(void) const override
            {
                return to_string(implementor);
            }

            /// Addendum : to enable polymorphic copy
            std::unique_ptr<stringable_concept> clone(void) const override
            {
                return std::unique_ptr<stringable_concept>(new stringable_model(*this));
            }
        private:
            Implementor implementor;
        };

    public:
        stringable(void) = default;

        //build from an implementor (move)
        template <typename Implementor>
        stringable(Implementor&& impl) noexcept
        : impl(new stringable_model<std::decay_t<Implementor>>(std::forward<Implementor>(impl)))
        {

        }

        //assign (move ) an implementor
        template <typename Implementor>
        stringable& operator=(Implementor&& impl) noexcept
        {
            impl.reset(new stringable_model<std::decay_t<Implementor>>(std::forward<Implementor>(impl)));
            return *this;
        }

        //free function to_string for Stringables ( to start the calls)
        friend std::string to_string(stringable const& stringable)
        {
            return stringable.impl->do_to_string();
        }

        //allow copy
        stringable(stringable const& rhs)
        : impl(rhs.impl->clone())
        {

        }
        stringable& operator= (stringable const& rhs)
        {
            impl = rhs.impl->clone();
        }

        //allow movement
        stringable(stringable&& rhs) noexcept = default;
        stringable& operator= (stringable&& rhs) noexcept = default;

    private:
        std::unique_ptr<stringable_concept> impl;
    };
}


#endif //KLONDIKE_PATTERNS_STRINGABLE_HPP
