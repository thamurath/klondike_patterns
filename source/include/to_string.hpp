#ifndef KLONDIKE_PATTERNS_TO_STRING_HPP
#define KLONDIKE_PATTERNS_TO_STRING_HPP

#include <string>
#include <utility>

/// Use notstd::to_string to convert any object to_string in the standard way
/// to enable this for any user type just define a function to_string for that type
/// inside the same namespace the type is declared.
namespace notstd
{
    namespace adl_helper
    {
        template <typename T>
        std::string as_string(T&& t)
        {
            return to_string(std::forward<T>(t));
        }
    }

    template <typename T>
    std::string to_string(T&& t)
    {
        return adl_helper::as_string(std::forward<T>(t));
    }
}

#endif //KLONDIKE_PATTERNS_TO_STRING_HPP
