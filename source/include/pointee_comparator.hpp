
#ifndef LEARNING_POINTEE_COMPARATOR_HPP
#define LEARNING_POINTEE_COMPARATOR_HPP

template<typename T>
struct pointee_comparator
{
    bool operator()(T const* lhs, T const* rhs) const
    {
        return *lhs<*rhs;
    }
};
#endif //LEARNING_POINTEE_COMPARATOR_HPP
