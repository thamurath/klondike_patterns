#ifndef KLONDIKE_PATTERNS_RETURNCODE_HPP
#define KLONDIKE_PATTERNS_RETURNCODE_HPP

#include <better_enums.hpp>

#include <limits>

BETTER_ENUM(ReturnCode, std::int32_t,
            EXIT_APP = std::numeric_limits<std::int32_t>::min(),
            ERROR = -1,
            SUCCESS = 0,
            VICTORY = std::numeric_limits<std::int32_t>::max()
);

#endif //KLONDIKE_PATTERNS_RETURNCODE_HPP
