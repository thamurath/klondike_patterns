#ifndef KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMANDBUILDER_HPP
#define KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class PileToFoundationCommandBuilder : public CommandBuilder
{
public:
    ~PileToFoundationCommandBuilder(void) override = default;

    std::unique_ptr<Command> Build(TableView &tableView) override;


    static std::string Description(void)
    {
        return "Pile to Foundation";
    }
};

#endif //KLONDIKE_PATTERNS_PILETOFOUNDATIONCOMMANDBUILDER_HPP
