#ifndef KLONDIKE_DECKVIEW_HPP
#define KLONDIKE_DECKVIEW_HPP

#include "ElementView.hpp"

class DeckView final : public ElementView
{
public:
    using ElementView::ElementView;

    ~DeckView(void) override = default;
    
    //allow movement
    DeckView(DeckView&& rhs) noexcept = default;
    DeckView& operator=(DeckView&& rhs) noexcept = default;

    void Decode(ViewStatus const &status) override;
    void ShowStatus(void) override;
    DeckView* Clone(void) const override;

protected:
private:
    //allow only private copy
    DeckView(DeckView const& rhs) = default;
    DeckView& operator=(DeckView const& rhs) = default;
};



#endif //KLONDIKE_DECKVIEW_HPP
