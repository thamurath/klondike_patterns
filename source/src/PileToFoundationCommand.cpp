#include "PileToFoundationCommand.hpp"

#include "TableModel.hpp"
#include "Card.hpp"


PileToFoundationCommand::PileToFoundationCommand(PileView& wasteView, FoundationView& foundationView)
        : pileView(pileView)
        , foundationView(foundationView)
{

}

ReturnCode PileToFoundationCommand::Check(TableModel &modelInterface)
{
    if (modelInterface.IsEmpty(pileView))
    {
        return ReturnCode::ERROR;
    }

    auto pileCard = modelInterface.GetCard(pileView);
    auto foundationCard = modelInterface.GetCard(foundationView);

    if (foundationCard)
    {// foundation is not empty
        // Only the next figure of the same suit can be put in.
        if ( (*pileCard).IsSameSuit(*foundationCard) && (*pileCard).IsNextTo(*foundationCard) )
        {
            return ReturnCode::SUCCESS;
        }
    }
    else // foundation is empty
    {
        // if foundation is empty only the first figure can be putted in.
        if ((*pileCard).IsFirst())
        {
            return ReturnCode::SUCCESS;
        }
    }
    return ReturnCode::ERROR;
}

ReturnCode PileToFoundationCommand::Execute(TableModel &modelInterface)
{
    return modelInterface.move(pileView, foundationView, 1);
}

std::vector<ElementView *> PileToFoundationCommand::GetInvolvedElements(void) const
{
    return {&pileView, &foundationView};
}

