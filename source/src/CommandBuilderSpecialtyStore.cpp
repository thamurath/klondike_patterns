#include "CommandBuilderSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxyBase.hpp"


CommandBuilderSpecialtyStore& CommandBuilderSpecialtyStore::Instance(void)
{
    static CommandBuilderSpecialtyStore instance;
    return instance;
}

void CommandBuilderSpecialtyStore::Register(CommandBuilderRegisterProxyBase *proxy)
{
    registeredCommandBuilders.push_back(proxy);
}

