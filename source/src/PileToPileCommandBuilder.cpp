#include "PileToPileCommandBuilder.hpp"
#include "PileToPileCommand.hpp"


#include "PileView.hpp"
#include "ElementView.hpp"

#include "IOTechSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxy.hpp"




static CommandBuilderRegisterProxy<PileToPileCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> PileToPileCommandBuilder::Build(TableView &tableView)
{
    auto piles = tableView.GetAllElements(ElementType::pile);
    assert(0 < std::size(piles));
    assert(nullptr != dynamic_cast<PileView*>(piles.front()));

    auto pileOptionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(piles);
    auto pileFromOption = pileOptionMenu->Execute("Choose one pile to move card from: ");

    auto cardNumberMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetAskForNumberMenu(1, piles.at(pileFromOption)->GetNumCards());
    auto numCards = cardNumberMenu->Execute("Choose number of cards to move from pile: ");

    decltype(pileFromOption) pileToOption;
    for (
            pileToOption = pileOptionMenu->Execute("Choose one pile to move cards to: ");
            pileToOption == pileFromOption;
            pileToOption = pileOptionMenu->Execute("Choose one pile to move cards to:")
            );

    return std::make_unique<PileToPileCommand>(
            *reinterpret_cast<PileView*>(piles.at(pileFromOption))
            , *reinterpret_cast<PileView*>(piles.at(pileToOption))
            , numCards
    );
}



