#include "WasteToPileCommand.hpp"

#include "TableModel.hpp"
#include "Card.hpp"


WasteToPileCommand::WasteToPileCommand(WasteView &wasteView, PileView &pileView)
: wasteView(wasteView)
, pileView(pileView)
{

}

ReturnCode WasteToPileCommand::Check(TableModel &modelInterface)
{
    if (modelInterface.IsEmpty(wasteView))
    {
        return ReturnCode::ERROR;
    }

    auto wastecard = modelInterface.GetCard(wasteView);
    auto pilecard = modelInterface.GetCard(pileView);
    ///@jlom TODO Here should check if the last card in Pile is FacedUp and not same color and one figure more than waste card
    if (pilecard)
    {// pile is not empty
        if ((*pilecard).IsTurnDown())
        {
            return ReturnCode::ERROR;
        }
        return (!(*wastecard).IsSameColor(*pilecard) && (*wastecard).IsNextTo(*pilecard))?ReturnCode::SUCCESS: ReturnCode::ERROR;
    }
    else // pile is empty
    {
        // only the last figure can be put down on an empty pile
        if ((*wastecard).IsLast())
        {
            return ReturnCode::SUCCESS;
        }
        return ReturnCode::ERROR;
    }
}

ReturnCode WasteToPileCommand::Execute(TableModel &modelInterface)
{
    return modelInterface.move(wasteView, pileView, 1);
}

std::vector<ElementView *> WasteToPileCommand::GetInvolvedElements(void) const
{
    return {&wasteView, &pileView};
}




