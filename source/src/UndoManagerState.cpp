#include "UndoManagerState.hpp"

namespace undo_manager
{
    State::State(std::unique_ptr<undo_manager::StateImpl> &&impl)
    : impl(std::move(impl))
    {

    }

    StateDispatcher State::operator->()
    {
        return *impl;
    }

    void State::Reset(std::unique_ptr<StateImpl> &&newImpl)
    {
        impl = std::move(newImpl);
    }
}
