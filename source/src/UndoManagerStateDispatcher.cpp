#include "UndoManagerStateDispatcher.hpp"

namespace undo_manager
{
    StateDispatcher::StateDispatcher(StateImpl &stateImpl)
    : stateImpl(stateImpl)
    {

    }

    StateDispatcher::~StateDispatcher()
    {
        stateImpl.updateCurrentState();
    }

    StateImpl *StateDispatcher::operator->()
    {
        return &stateImpl;
    }
}

