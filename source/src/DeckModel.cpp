#include "DeckModel.hpp"

#include <cassert>
DeckModel::DeckModel(std::string&& id, ElementType&& type, std::vector<Card>&& cards)
 : ElementModel(std::forward<std::string>(id), std::forward<ElementType>(type), std::forward<std::decay_t<decltype(cards)>>(cards))
{

}

ElementStatus DeckModel::SaveStatus(void) const
{
    return ElementStatus{cards};
}

void DeckModel::LoadStatus(ElementStatus const &status)
{
    cards = status.GetCards();
}

void DeckModel::AddCard(Card &&card)
{
    //@jlom TODO montar la carta para poder hacer todas estas comprobaciones
    //assert(card.IsFaceDown());
    ElementModel::AddCard(std::forward<Card>(card));
}

void DeckModel::ExtractCard(void)
{
    assert(false == cards.empty());
    ElementModel::ExtractCard();
}

std::optional<Card> DeckModel::GetCard(void) const
{
    assert(cards.size() > 0);
    return ElementModel::GetCard();
}

std::vector<Card> DeckModel::GetCards(std::size_t numCards) const
{
    // Do we really should allow to someone to ask for more than one card?
    assert(false);
}

std::vector<Card> DeckModel::GetAllCards(void) const
{
    // same as GetCards
    assert(false);
    return ElementModel::GetAllCards();
}
