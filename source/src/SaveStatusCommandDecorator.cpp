#include "SaveStatusCommandDecorator.hpp"
#include "UndoManager.hpp"
#include "ModelStateBuilder.hpp"

ReturnCode SaveStatusCommandDecorator::Execute(TableModel &modelInterface)
{
    saveCurrentStatus(modelInterface);
    return cmd->Execute(modelInterface);
}

void SaveStatusCommandDecorator::saveCurrentStatus(TableModel &modelInterface) const
{
    auto involvedElements = cmd->GetInvolvedElements();
    ModelStateBuilder modelStateBuilder;
    for (auto& element : involvedElements)
    {
        auto status = modelInterface.saveElementState(*element);
        modelStateBuilder.addElement(ModelStateBuilder::value_type{element, std::move(*status)});
    }
    undo_manager::UndoManager::Instance().to_do(modelStateBuilder.Build());
}
