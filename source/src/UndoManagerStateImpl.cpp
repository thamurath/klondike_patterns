#include "UndoManagerStateImpl.hpp"
#include "UndoManagerContext.hpp"


namespace undo_manager
{
    StateImpl::StateImpl(Context& context)
    : context(context)
    {

    }

    Context& StateImpl::getContext(void) const
    {
        return context;
    }
}

