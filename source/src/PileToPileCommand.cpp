#include "PileToPileCommand.hpp"

#include "TableModel.hpp"
#include "Card.hpp"

#include <algorithm>

PileToPileCommand::PileToPileCommand(PileView &pileViewFrom, PileView &pileViewTo, std::uint8_t numCards)
        : pileViewFrom(pileViewFrom)
        , pileViewTo(pileViewTo)
        , numCards(numCards)
{

}

ReturnCode PileToPileCommand::Check(TableModel &modelInterface)
{
    if (numCards < modelInterface.GetNumCards(pileViewFrom) )
    {
        return ReturnCode::ERROR;
    }

    auto cards = modelInterface.GetCards(pileViewFrom, numCards);
    if (! std::all_of(std::begin(cards), std::end(cards), [](auto const& card){ return card.IsTurnUp();}) )
    {
        return ReturnCode::ERROR;
    }

    auto& lastCard = cards.at(std::size(cards)-1);

    auto pileCard = modelInterface.GetCard(pileViewTo);

    if(pileCard)
    {// destination pile is not empty
        // card should be next figure and different color.
        if ( lastCard.IsSameColor(*pileCard) && lastCard.IsNextTo(*pileCard) )
        {
            return ReturnCode::SUCCESS;
        }
    }
    else // destination pile is empty
    {
        // only the last figure can be put in.
        if ( lastCard.IsLast() )
        {
            return ReturnCode::SUCCESS;
        }
    }

    return ReturnCode::ERROR;
}

ReturnCode PileToPileCommand::Execute(TableModel &modelInterface)
{
    return modelInterface.move(pileViewFrom, pileViewTo, numCards);
}

std::vector<ElementView *> PileToPileCommand::GetInvolvedElements(void) const
{
    return {&pileViewFrom, &pileViewTo};
}


