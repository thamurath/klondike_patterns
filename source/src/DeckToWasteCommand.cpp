#include "DeckToWasteCommand.hpp"
#include "TableModel.hpp"


DeckToWasteCommand::DeckToWasteCommand(DeckView *deckView, WasteView *wasteView)
: deckView(deckView)
, wasteView(wasteView)
{

}
ReturnCode DeckToWasteCommand::Execute(TableModel &modelInterface)
{
   return modelInterface.move(*deckView, *wasteView, 1, [](Card& c){c.TurnUp();} );
}

ReturnCode DeckToWasteCommand::Check(TableModel &modelInterface)
{
    if ( modelInterface.IsEmpty(*deckView) )
    {
        return ReturnCode::ERROR;
    }

    return ReturnCode::SUCCESS;
}

std::vector<ElementView *> DeckToWasteCommand::GetInvolvedElements(void) const
{
    return {deckView, wasteView};
}


