#include "ModelStateComposite.hpp"

void ModelStateComposite::load(TableModel &modelInterface)
{
    for (auto& child : childs)
    {
        child->load(modelInterface);
    }
}

void ModelStateComposite::add(std::unique_ptr<ModelState> modelState)
{
    childs.push_back(std::move(modelState));

}

