#include "UndoManagerUndoingRedoingStateImpl.hpp"
#include "UndoManagerContext.hpp"
#include "UndoManagerDoingStateImpl.hpp"

namespace undo_manager
{
    UndoingRedoingStateImpl::UndoingRedoingStateImpl(Context &context)
            : StateImpl(context)
            , shouldChange(false)
    {

    }

    void UndoingRedoingStateImpl::to_do(std::unique_ptr<ModelState> modelState)
    {
        // delete all the states from the mark+1 ( mark is on current state)
        getContext().StatePool().erase(getContext().Mark().base(), std::end(getContext().StatePool()));

        // now insert the new state
        getContext().StatePool().push_back(std::move(modelState));

        //and reset the mark
        getContext().Mark() = getContext().StatePool().rend();

        shouldChange = true;
    }

    ModelState const& UndoingRedoingStateImpl::to_undo(void)
    {
        if(getContext().StatePool().rend() == getContext().Mark())
        {
            throw "Error";
        }
        ++getContext().Mark();
        return *(*(getContext().Mark()));

    }

    ModelState const& UndoingRedoingStateImpl::to_redo(void)
    {
        if(getContext().StatePool().rbegin() == getContext().Mark())
        {
            throw "Error";
        }
        --getContext().Mark();
        return *(*(getContext().Mark()));
    }

    void UndoingRedoingStateImpl::updateCurrentState(void) const
    {
        if(shouldChange)
        {
            getContext().CurrentState().Reset(std::make_unique<DoingStateImpl>(getContext()));
        }
    }
}


