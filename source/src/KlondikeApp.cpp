#include <source/ModelStateBuilder.hpp>
#include "KlondikeApp.hpp"

#include "UndoManager.hpp"
#include "CommandBuilderManager.hpp"
#include "TableView.hpp"
#include "ControlInterface.hpp"
#include "ReturnCode.hpp"

ReturnCode KlondikeApp::Run(int argc, char **argv)
{

    TableModel tableModel;
    ControlInterface controlInterface(tableModel);
    CommandBuilderManager builderManager;
    TableView tableView(builderManager, controlInterface);


    InitializeUndoManager(tableModel, tableView);

    return  tableView.Run();

}

void KlondikeApp::InitializeUndoManager(TableModel &tableModel, TableView &tableView) const
{
    ModelStateBuilder modelbuilder;
    for ( auto& view : tableView.GetAllElements())
    {
        modelbuilder.addElement(ModelStateBuilder::value_type{view, *tableModel.saveElementState(*view)});
    }
    undo_manager::UndoManagerBuilder managerBuilder;
    managerBuilder.CreateUndoManager(modelbuilder.Build());
}
