#include "TerminalNumberedOptionMenu.hpp"

#include <to_string.hpp>
#include <iostream>

std::uint32_t TerminalNumberedOptionMenu::Execute(std::string questionLabel)
{
    for (std::size_t idx(0); idx < options.size(); ++idx)
    {
        std::cout << idx+1 << ": " << notstd::to_string(options.at(idx)) << '\n';
    }
    std::uint32_t num(0);

    do{
        std::cout << '\n' << questionLabel << std::endl;
        if (std::cin >> num)
        {
            if ( (num < 1) || (num > options.size()+1))
            {
                std::cout << "Wrong limits, try again";
            }
            else
            {
                break;
            }
        }
        else
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            std::cout << "Wrong input, try again";
        }
    }while (true);

    return num;

}