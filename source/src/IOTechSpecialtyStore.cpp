#include "IOTechSpecialtyStore.hpp"
#include "IOTechRegisterProxyBase.hpp"

#include "ConfigurationErrorException.hpp"

#include <cassert>

void IOTechSpecialtyStore::Register(IOTechRegisterProxyBase* const proxy)
{
    registeredFactories.push_back(proxy);
}

IOTechAbstractFactory& IOTechSpecialtyStore::GetIOTech(void) const
{
    ///todo get choosen factory name from configuration
    constexpr char configuredFactoryName[] = "terminal";

    if (! factory)
    {
        for (auto& fac : registeredFactories)
        {
            if (configuredFactoryName == fac->GetAbstractFactoryType())
            {
                factory = fac->CreateObject();
                break;
            }
        }
        if(!factory)
        {
            throw exceptions::ConfigurationErrorException(std::string("Unkown IOTech configured: ") + configuredFactoryName);
        }
    }

    return *factory;
}
