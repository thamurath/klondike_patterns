#include "JsonGameStateEncoderVisitor.hpp"


#include "ElementGameStatus.hpp"
#include "ElementGameStatusWithId.hpp"
#include "AllElementsGameStatus.hpp"

#include "ModelStateComposite.hpp"
#include "ModelStateLeaf.hpp"

#include "stringable.hpp"


concepts::stringable JsonGameStateEncoderVisitor::Visit(ModelStateLeaf &modelState)
{
    return  ElementGameStatusWithId (modelState.GetElementView().getId()
            , modelState.GetElementStatus().Accept(*this)
    );
}

concepts::stringable JsonGameStateEncoderVisitor::Visit(ModelStateComposite &modelState)
{
    AllElementsGameStatus allElementsGameStatus;
    for (auto& child : modelState.GetChilds() )
    {
        allElementsGameStatus.Add(child->Accept(*this) );
    }

    return allElementsGameStatus;
}

concepts::stringable JsonGameStateEncoderVisitor::Visit(ElementStatus &elementStatus)
{
    std::vector<CardView> cards;
    for (auto& element: elementStatus.GetCards() )
    {
        cards.push_back(element.CreateCardView());
    }
    ElementGameStatus st(cards);
    return st;
}


