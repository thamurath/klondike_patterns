#include "ControlInterface.hpp"
#include "SaveStatusCommandDecorator.hpp"

#include <utility>
#include <cassert>

ControlInterface::ControlInterface(TableModel &tableModel)
: tableModel(tableModel)
{

}

ReturnCode ControlInterface::ExecuteCommand(std::unique_ptr<Command> cmd)
{
    SaveStatusCommandDecorator saveStatusComand(std::move(cmd));
    auto retCode = saveStatusComand.Check(tableModel);
    if (+(ReturnCode::SUCCESS) == retCode)
    {
        retCode = saveStatusComand.Execute(tableModel);
    }
    return retCode;
}
