#include "PileToFoundationCommandBuilder.hpp"
#include "PileToFoundationCommand.hpp"

#include "WasteView.hpp"
#include "FoundationView.hpp"
#include "ElementView.hpp"

#include "IOTechSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxy.hpp"




static CommandBuilderRegisterProxy<PileToFoundationCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> PileToFoundationCommandBuilder::Build(TableView &tableView)
{
    auto piles = tableView.GetAllElements(ElementType::pile);
    assert(0 < std::size(piles));
    assert(nullptr != dynamic_cast<PileView*>(piles.front()));

    auto foundations = tableView.GetAllElements(ElementType::foundation);
    assert(0 < std::size(foundations));
    assert(nullptr != dynamic_cast<FoundationView*>(foundations.front()));

    auto pileOptionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(piles);
    auto pileOption = pileOptionMenu->Execute("Choose one pile to move card from: ");

    auto foundationOptionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(foundations);
    auto foundationOption = foundationOptionMenu->Execute("Choose one foundation to move card to: ");

    return std::make_unique<PileToFoundationCommand>(
            *reinterpret_cast<PileView*>(piles.at(pileOption))
            , *reinterpret_cast<FoundationView*>(foundations.at(foundationOption))
    );
}


