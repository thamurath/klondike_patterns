#include "DeckToWasteCommandBuilder.hpp"
#include "CommandBuilderRegisterProxy.hpp"

#include "DeckToWasteCommand.hpp"
#include "ElementType.hpp"

#include <string>
#include <cassert>

static CommandBuilderRegisterProxy<DeckToWasteCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> DeckToWasteCommandBuilder::Build(TableView &tableView)
{
    auto deckContainer = tableView.GetAllElements(ElementType::deck);
    assert(1 == std::size(deckContainer));
    assert(nullptr != dynamic_cast<DeckView*>(deckContainer.front()));

    auto wasteContainer = tableView.GetAllElements(ElementType::waste);
    assert(1 == std::size(wasteContainer));
    assert(nullptr != dynamic_cast<WasteView*>(wasteContainer.front()));

    return std::make_unique<DeckToWasteCommand>(reinterpret_cast<DeckView*>(deckContainer.front())
            , reinterpret_cast<WasteView*>(wasteContainer.front()));
}
