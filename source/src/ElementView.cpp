#include "ElementView.hpp"

#include <utility>
#include <cassert>

ElementView::ElementView(std::string&& id, ElementType&& type)
        : Element(std::forward<std::string>(id), std::forward<ElementType>(type))
        , statusTranscoder(nullptr)
{

}

ElementView::~ElementView(void)
{
    delete statusTranscoder;
}

//std::string const& ElementView::getId(void) const
//{
//    return id;
//}
//
//ElementType const& ElementView::getType(void) const
//{
//    return type;
//}

void ElementView::SetStatusTranscoder(StatusTranscoder* statusTranscoder)
{
    statusTranscoder = std::move(statusTranscoder);
}

StatusTranscoder& ElementView::GetStatusTranscoder(void) const
{
    assert(nullptr != statusTranscoder);
    return *statusTranscoder;
}



std::string to_string(ElementView *& elementView)
{
    return elementView->getId();
}
