#include "WasteToFoundationCommandBuilder.hpp"
#include "WasteToFoundationCommand.hpp"

#include "WasteView.hpp"
#include "FoundationView.hpp"
#include "ElementView.hpp"

#include "IOTechSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxy.hpp"




static CommandBuilderRegisterProxy<WasteToFoundationCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> WasteToFoundationCommandBuilder::Build(TableView &tableView)
{
    auto wasteContainer = tableView.GetAllElements(ElementType::waste);
    assert(1 == std::size(wasteContainer));
    assert(nullptr != dynamic_cast<WasteView*>(wasteContainer.front()));

    auto foundations = tableView.GetAllElements(ElementType::foundation);
    assert(0 < std::size(foundations));
    assert(nullptr != dynamic_cast<FoundationView*>(foundations.front()));

    auto optionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(foundations);
    auto option = optionMenu->Execute("Choose one foundation to move card to: ");

    return std::make_unique<WasteToFoundationCommand>(
            *reinterpret_cast<WasteView*>(wasteContainer.front())
            , *reinterpret_cast<FoundationView*>(foundations.at(option))
    );
}

