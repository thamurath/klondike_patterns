#include "IOTechRegisterProxyBase.hpp"
#include "IOTechSpecialtyStore.hpp"


IOTechRegisterProxyBase::IOTechRegisterProxyBase(void)
{
    IOTechSpecialtyStore::Instance().Register(this);
}
