#include "WasteModel.hpp"

#include <cassert>

ElementStatus WasteModel::SaveStatus(void) const
{
    return ElementStatus(cards);
}

void WasteModel::LoadStatus(const ElementStatus &status)
{
    cards = status.GetCards();
}

void WasteModel::AddCard(Card &&card)
{
    ///@jlom TODO create card so we can check all these things
    //assert(card.IsFaceUp());
    ElementModel::AddCard(std::forward<Card>(card));
}

void WasteModel::ExtractCard(void)
{
    assert(!cards.empty());
    ElementModel::ExtractCard();
}

std::optional <Card> WasteModel::GetCard(void) const
{
    assert(!cards.empty());
    return ElementModel::GetCard();
}

std::vector <Card> WasteModel::GetCards(std::size_t numCards) const
{
    assert(cards.size() >= numCards);
    return ElementModel::GetCards(numCards);
}

