#include "WasteToPileCommandBuilder.hpp"
#include "WasteToPileCommand.hpp"

#include "WasteView.hpp"
#include "PileView.hpp"
#include "ElementView.hpp"

#include "IOTechSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxy.hpp"




static CommandBuilderRegisterProxy<WasteToPileCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> WasteToPileCommandBuilder::Build(TableView &tableView)
{
    auto wasteContainer = tableView.GetAllElements(ElementType::waste);
    assert(1 == std::size(wasteContainer));
    assert(nullptr != dynamic_cast<WasteView*>(wasteContainer.front()));

    auto piles = tableView.GetAllElements(ElementType::pile);
    assert(0 < std::size(piles));
    assert(nullptr != dynamic_cast<PileView*>(piles.front()));

    auto optionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(piles);
    auto option = optionMenu->Execute("Choose one pile to move card to: ");

    return std::make_unique<WasteToPileCommand>(
            *reinterpret_cast<WasteView*>(wasteContainer.front())
            , *reinterpret_cast<PileView*>(piles.at(option))
    );
}
