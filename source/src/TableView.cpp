#include "TableView.hpp"

#include "ElementViewIteratorAttorney.hpp"

#include "DeckView.hpp"
#include "WasteView.hpp"
#include "PileView.hpp"
#include "FoundationView.hpp"

#include "CommandBuilderManager.hpp"
#include "ControlInterface.hpp"


#include <utility>
#include <cassert>
#include <source/IOTechSpecialtyStore.hpp>

#include "ElementsBuilder.hpp"

TableView::TableView(CommandBuilderManager &builderManager, ControlInterface &controlInterface)
        : builderManager(builderManager)
        , controlInterface(controlInterface)
        , views (std::move(ElementsBuilder::Instance().GetElementViews()))
{
    for (auto& v : views)
    {
        by_id.insert({v->getId(), v.get()});
        by_type.insert({v->getType(), v.get()});
    }
}
ElementViewCollectionNotOwning TableView::GetAllElements(void)
{
    ElementViewCollectionNotOwning ret;
    ret.reserve(views.size());
    for(auto& v : views)
    {
        ret.push_back(v.get());
    }
    return ret;
}

ElementViewCollectionNotOwning TableView::GetAllElements(ElementType const &type)
{
    ElementViewCollectionNotOwning retVal;
    auto range = by_type.equal_range(type);
    for (auto it=range.first; it != range.second; ++it)
    {
        retVal.push_back(std::forward<decltype(it->second)>(it->second));
    }

    return retVal;
}

ElementView& TableView::GetElement(std::string const &id)
{
    auto it = by_id.find(id);
    return *(it->second);
}

ReturnCode TableView::Run(void)
{
    ReturnCode returnCode = ReturnCode::SUCCESS;
    do
    {
        auto availableCommands = builderManager.GetAvailableCommands();
        auto optionMenu = IOTechSpecialtyStore::Instance().GetIOTech().GetNumberedOptionMenu(availableCommands);
        auto choosenOption = optionMenu->Execute("Give me an option");
        returnCode = controlInterface.ExecuteCommand(availableCommands[choosenOption]->CreateCommandBuilder()->Build(*this));
    } while(notExit(returnCode));

    return returnCode;
}

bool TableView::notExit(ReturnCode const& returnCode) const
{
    return (+(ReturnCode::VICTORY) == returnCode) || (+(ReturnCode::EXIT_APP) == returnCode);
}

