#include "CommandBuilderRegisterProxyBase.hpp"

#include "CommandBuilderSpecialtyStore.hpp"

CommandBuilderRegisterProxyBase::CommandBuilderRegisterProxyBase(void)
{
    CommandBuilderSpecialtyStore::Instance().Register(this);
}
