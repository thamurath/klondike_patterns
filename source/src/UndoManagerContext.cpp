#include "UndoManagerContext.hpp"

#include <utility>
#include <memory>

namespace undo_manager
{
    Context::Context(std::unique_ptr<StateImpl> &&state)
            : statePool()
            , mark(std::end(statePool))
            , currentState(std::make_unique<State>(std::move(state)))
    {

    }

    void Context::SetInitialState(std::unique_ptr<ModelState> initialState)
    {
        statePool.push_back(std::move(initialState));
    }

    Context::StatePoolType::reverse_iterator& Context::Mark(void)
    {
        return mark;
    }

    State& Context::CurrentState(void)
    {
        return *currentState;
    }

    Context::StatePoolType& Context::StatePool(void)
    {
        return statePool;
    }
}

