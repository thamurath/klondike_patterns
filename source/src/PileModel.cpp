#include "PileModel.hpp"

#include <cassert>

ElementStatus PileModel::SaveStatus(void) const
{
    return ElementStatus(cards);
}

void PileModel::LoadStatus(ElementStatus const &status)
{
    cards = status.GetCards();
}

void PileModel::AddCard(Card &&card)
{
    ///@jlom Todo what to assert here?
    ElementModel::AddCard(std::forward<Card>(card));
}

void PileModel::ExtractCard(void)
{
    assert(!cards.empty());
    ElementModel::ExtractCard();
}

std::optional<Card> PileModel::GetCard(void) const
{
    assert(!cards.empty());
    return ElementModel::GetCard();
}

std::vector<Card> PileModel::GetCards(std::size_t numCards) const
{
    assert(cards.size() >= numCards);
    return ElementModel::GetCards(numCards);
}


