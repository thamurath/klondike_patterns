#include "TableModel.hpp"
#include "ElementsBuilder.hpp"

#include <functional>

TableModel::TableModel(void)
        : models(std::move(ElementsBuilder::Instance().GetElements()))
{
    for (auto& m : models)
    {
        by_id.insert({m->getId(), m.get()});
        by_type.insert({m->getType(), m.get()});
    }
}

void TableModel::loadElementState(ElementView &elementView, ElementStatus &status)
{
    auto elementModelIt = by_id.find(elementView.getId());
    if (std::end(by_id) == elementModelIt)
    {
        // error: Element not found
        return;
    }

    elementModelIt->second->LoadStatus(status);
}

std::optional<ElementStatus> TableModel::saveElementState(ElementView &elementView)
{
    auto elementModelIt = by_id.find(elementView.getId());
    if (std::end(by_id) == elementModelIt)
    {
        // error: Element not found
        return {};
    }

    return {elementModelIt->second->SaveStatus()};
}

ReturnCode TableModel::move(ElementView &from, ElementView &to, const std::uint8_t &num_cards, std::function<void(Card&)> modifyFunctor)
{
    auto source = findById(from.getId());
    auto destination = findById(to.getId());

    auto card = source->GetCard();
    modifyFunctor(*card);
    destination->AddCard(std::move(*card));

    return ReturnCode::SUCCESS;
}



std::optional<Card> TableModel::GetCard(ElementView &elementView) const
{
    return findById(elementView.getId())->GetCard();
}

std::vector<Card> TableModel::GetCards(ElementView &elementView, size_t numCards) const
{
    return findById(elementView.getId())->GetCards(numCards);
}

std::size_t TableModel::GetNumCards(ElementView &elementView) const
{
    return findById(elementView.getId())->GetNumCards();
}

bool TableModel::IsEmpty(ElementView &elementView) const
{
    return findById(elementView.getId())->IsEmpty();
}

std::vector<Card> TableModel::GetAllCards(ElementView &elementView) const
{
    return findById(elementView.getId())->GetAllCards();
}


ElementModel* TableModel::findById(std::string id) const
{
    auto element = by_id.find(id);
    if (std::end(by_id) == element)
    {
        return nullptr;
    }
    return element->second;
}