#include "KlondikeApp.hpp"

auto main(int argc, char** argv) ->int
{
    KlondikeApp app;
    app.Run(argc, argv);
    return 0;
}

