#include "ModelStateBuilder.hpp"
#include "ModelStateComposite.hpp"
#include "ModelStateLeaf.hpp"

ModelStateBuilder& ModelStateBuilder::addElement(value_type&& element)
{
    elements.push_back(std::move(element));
    return *this;
}

std::unique_ptr<ModelState> ModelStateBuilder::Build(void)
{
    if( 1 == elements.size())
    {
        return std::make_unique<ModelStateLeaf>(*std::get<0>(elements.front()), std::move(std::get<1>(elements.front())));
    }
    else
    {
        auto composite = std::make_unique<ModelStateComposite>();
        for (auto& element : elements)
        {
            composite->add(std::make_unique<ModelStateLeaf>(*std::get<0>(element), std::move(std::get<1>(element))));
        }
        //return std::move(composite);
        return composite;
    }
}