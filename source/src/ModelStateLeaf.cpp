#include "ModelStateLeaf.hpp"

#include <utility>


ModelStateLeaf::ModelStateLeaf(ElementView& elementView, ElementStatus cardHeapStatus)
: elementView(elementView.Clone())
, cardHeapStatus(std::move(cardHeapStatus))
{

}

ModelStateLeaf::~ModelStateLeaf()
{
    delete elementView;
}

void ModelStateLeaf::load(TableModel &modelInterface)
{
    modelInterface.loadElementState(*elementView, cardHeapStatus);
}




