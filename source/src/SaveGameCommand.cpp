#include "SaveGameCommand.hpp"

#include "ModelStateBuilder.hpp"
#include "JsonGameStateEncoderVisitor.hpp"

#include <utility>
#include <fstream>


SaveGameCommand::SaveGameCommand(std::string filename, ElementViewCollectionNotOwning viewCollection)
        : filename(filename)
        , viewCollection(std::move(viewCollection))
{

}

ReturnCode SaveGameCommand::Check(TableModel &modelInterface)
{
    // you always can save a game ...
    return ReturnCode::SUCCESS;
}

ReturnCode SaveGameCommand::Execute(TableModel &modelInterface)
{// todo implement this
    ModelStateBuilder modelStateBuilder;
    for ( auto ite = viewCollection.begin(); ite != viewCollection.end(); ++ite)
    {
        auto status = modelInterface.saveElementState(**ite);
        modelStateBuilder.addElement(ModelStateBuilder::value_type{*ite, std::move(*status)});
    }
    JsonGameStateEncoderVisitor visitor;

    std::ofstream textfile;
    textfile.open(filename);
    textfile << to_string(modelStateBuilder.Build()->Accept(visitor));
    textfile.close();

    return ReturnCode::SUCCESS;
}

std::vector<ElementView *> SaveGameCommand::GetInvolvedElements(void) const
{
    //no views need to be saved
    return {};
}


