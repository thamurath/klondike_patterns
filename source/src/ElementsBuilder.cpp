#include "ElementsBuilder.hpp"

#include "DeckView.hpp"
#include "WasteView.hpp"
#include "FoundationView.hpp"
#include "PileView.hpp"

#include "DeckModel.hpp"
#include "WasteModel.hpp"
#include "PileModel.hpp"
#include "FoundationModel.hpp"

#include "CardFactory.hpp"
#include "ElementType.hpp"

#include <memory>
#include <source/ElementType.hpp>

ElementViewCollection ElementsBuilder::GetElementViews(void)
{
    // Estos datos deberian de ser leidos de un fichero de configuration.
    // El mismo conjunto de datos de configuracion daria lugar a la creacion de estos tipos
    // y a la creacion de los tipos para el modelo, de manera que fueran coherentes.

    ElementViewCollection collection;

    collection.push_back(std::make_unique<DeckView>("deck", ElementType::deck));
    collection.push_back(std::make_unique<WasteView>("waste", ElementType::waste));

    collection.push_back(std::make_unique<PileView>("pile_1", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_2", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_3", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_4", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_5", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_6", ElementType::pile));
    collection.push_back(std::make_unique<PileView>("pile_7", ElementType::pile));

    collection.push_back(std::make_unique<FoundationView>("foundation_1", ElementType::foundation));
    collection.push_back(std::make_unique<FoundationView>("foundation_2", ElementType::foundation));
    collection.push_back(std::make_unique<FoundationView>("foundation_3", ElementType::foundation));
    collection.push_back(std::make_unique<FoundationView>("foundation_4", ElementType::foundation));

    return collection;
}

ElementCollection ElementsBuilder::GetElements(void)
{

    // esto se construye con la misma informacion de configuracion  que las vistas, de manera que siempre son
    // coherentes entre ellas, tanto en tipo como en numero como en identificador de cada una.

    ElementCollection collection;

    auto deck = CardFactory::Instance().CreateDeck();

    ///@jlom TODO ... shuffle deck cards and pass some to piles.

    collection.emplace_back(std::make_unique<PileModel>("pile_1", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_2", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_3", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_4", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_5", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_6", ElementType::pile));
    collection.emplace_back(std::make_unique<PileModel>("pile_7", ElementType::pile));

    collection.push_back(std::make_unique<DeckModel>("deck", ElementType::deck, std::move(deck)));

    collection.emplace_back(std::make_unique<WasteModel>("waste", ElementType::waste));

    collection.emplace_back(std::make_unique<FoundationModel>("foundation_1", ElementType::foundation));
    collection.emplace_back(std::make_unique<FoundationModel>("foundation_2", ElementType::foundation));
    collection.emplace_back(std::make_unique<FoundationModel>("foundation_3", ElementType::foundation));
    collection.emplace_back(std::make_unique<FoundationModel>("foundation_4", ElementType::foundation));


    return collection;
}

ElementsBuilder& ElementsBuilder::Instance(void)
{
    static ElementsBuilder instance;
    return instance;
}
