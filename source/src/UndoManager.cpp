#include "UndoManager.hpp"

#include "UndoManagerDoingStateImpl.hpp"

#include <utility>
#include <cassert>

namespace undo_manager
{
    UndoManager::UndoManager(void)
    : context(std::make_unique<DoingStateImpl>(context))
    , initialized(false)
    {

    }

    void UndoManager::SetInitialState(std::unique_ptr<ModelState> modelState)
    {
        initialized = true;
        context.SetInitialState(std::move(modelState));
    }

    void UndoManager::to_do(std::unique_ptr<ModelState> modelState)
    {
        assert(initialized);
        context.CurrentState()->to_do(std::move(modelState));
    }

    ModelState const& UndoManager::to_undo(void)
    {
        assert(initialized);
        return context.CurrentState()->to_undo();
    }

    ModelState const& UndoManager::to_redo(void)
    {
        assert(initialized);
        return context.CurrentState()->to_redo();
    }
}

