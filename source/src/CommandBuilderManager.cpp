#include "CommandBuilderManager.hpp"
#include "CommandBuilder.hpp"
#include "CommandBuilderSpecialtyStore.hpp"


#include <utility>

CommandBuilderContainer CommandBuilderManager::GetAvailableCommands(void)
{
    return CommandBuilderSpecialtyStore::Instance().GetRegisteredElements();
}

