#include "FoundationModel.hpp"

#include <cassert>
ElementStatus FoundationModel::SaveStatus(void) const
{
    return ElementStatus(cards);
}

void FoundationModel::LoadStatus(ElementStatus const &status)
{
    cards = status.GetCards();
}

void FoundationModel::AddCard(Card &&card)
{
    ElementModel::AddCard(std::forward<Card>(card));
}

void FoundationModel::ExtractCard(void)
{
    assert(!cards.empty());
    ElementModel::ExtractCard();
}

std::optional<Card> FoundationModel::GetCard(void) const
{
    assert(!cards.empty());
    return ElementModel::GetCard();
}

std::vector<Card> FoundationModel::GetCards(std::size_t numCards) const
{
    assert(cards.size() >= numCards);
    return ElementModel::GetCards(numCards);
}

std::vector<Card> FoundationModel::GetAllCards(void) const
{
    assert(!cards.empty());
    return ElementModel::GetAllCards();
}
