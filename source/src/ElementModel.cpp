#include "ElementModel.hpp"

#include <algorithm>

ElementModel::ElementModel(std::string&& id, ElementType&& type, std::vector<Card>&& cards)
: Element(std::forward<std::string>(id), std::forward<ElementType>(type))
, cards(std::move(cards))
{

}

void ElementModel::AddCard(Card &&card)
{
    cards.push_back(std::move(card));
}

void ElementModel::ExtractCard(void)
{
    cards.pop_back();
}

std::optional<Card> ElementModel::GetCard(void) const
{
    if(cards.empty())
    {
        return {};
    }
    return cards.back();
}

std::vector<Card> ElementModel::GetCards(size_t numCards) const
{
    std::vector<Card> ret;
    ret.reserve(numCards);
    std::size_t num = std::min(static_cast<std::size_t>(numCards), cards.size());
    const std::size_t diff = cards.size() - num;
    std::copy(std::begin(cards) + diff, std::end(cards), std::back_inserter(ret));
    return ret;
}

std::size_t ElementModel::GetNumCards(void) const
{
    return cards.size();
}

bool ElementModel::IsEmpty(void) const
{
    return cards.empty();
}

std::vector<Card> ElementModel::GetAllCards(void) const
{
    return GetCards(cards.size());
}

//template <typename Card>
//bool CardHeap<Card>::IsEmpty(void) const
//{
//    return cards.empty();
//}
//
//template <typename Card>
//std::size_t CardHeap<Card>::Size(void) const
//{
//    return cards.size();
//}
//template <typename Card>
//std::optional<Card> CardHeap<Card>::Get(void) const
//{
//    if (cards.empty())
//    {
//        return {};
//    }
//    return cards.back();
//}
//
//template <typename Card>
//std::vector<Card> CardHeap<Card>::GetCards(std::size_t const& numCards) const
//{
//    std::vector<Card> ret;
//    ret.reserve(numCards);
//    std::size_t num = std::min(numCards, cards.size());
//    const std::size_t diff = cards.size() - num;
//    std::copy(std::begin(cards) + diff, std::end(cards), std::back_inserter(ret));
//    return ret;
//}
//
//template <typename Card>
//std::vector<Card> CardHeap<Card>::GetAllCards(void) const
//{
//    return GetCards(cards.size());
//}





