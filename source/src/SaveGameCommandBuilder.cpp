#include "SaveGameCommandBuilder.hpp"

#include "SaveGameCommand.hpp"

#include "IOTechSpecialtyStore.hpp"
#include "CommandBuilderRegisterProxy.hpp"

static CommandBuilderRegisterProxy<SaveGameCommandBuilder> autoRegisterProxy;

std::unique_ptr<Command> SaveGameCommandBuilder::Build(TableView &tableView)
{
    auto menu = IOTechSpecialtyStore::Instance().GetIOTech().GetAskForStringMenu();
    std::string filename = menu->Execute("Filename to save: ");
    return std::make_unique<SaveGameCommand>(filename,tableView.GetAllElements());
}