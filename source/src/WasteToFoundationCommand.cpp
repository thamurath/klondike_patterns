#include "WasteToFoundationCommand.hpp"

#include "TableModel.hpp"
#include "Card.hpp"


WasteToFoundationCommand::WasteToFoundationCommand(WasteView &wasteView, FoundationView& foundationView)
        : wasteView(wasteView)
        , foundationView(foundationView)
{

}

ReturnCode WasteToFoundationCommand::Check(TableModel &modelInterface)
{
    if (modelInterface.IsEmpty(wasteView))
    {
        return ReturnCode::ERROR;
    }

    auto wastecard = modelInterface.GetCard(wasteView);
    auto foundationCard = modelInterface.GetCard(foundationView);


    if (foundationCard)
    {// foundation is not empty
        // Only the next figure of the same suit can be put in.
        if ( (*wastecard).IsSameSuit(*foundationCard) && (*wastecard).IsNextTo(*foundationCard) )
        {
            return ReturnCode::SUCCESS;
        }
    }
    else // foundation is empty
    {
        // if foundation is empty only the first figure can be putted in.
        if ((*wastecard).IsFirst())
        {
            return ReturnCode::SUCCESS;
        }
    }
    return ReturnCode::ERROR;
}

ReturnCode WasteToFoundationCommand::Execute(TableModel &modelInterface)
{
    return modelInterface.move(wasteView, foundationView, 1);
}

std::vector<ElementView *> WasteToFoundationCommand::GetInvolvedElements(void) const
{
    return {&wasteView, &foundationView};
}

