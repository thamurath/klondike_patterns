#include "TerminalAskForStringMenu.hpp"

#include <iostream>


std::string TerminalAskForStringMenu::Execute(std::string questionLabel)
{
    std::string str;
    do{
        std::cout << '\n' << questionLabel;
        if (min_size > 0)
        {
            std::cout <<  "(min:" << min_size << ")";
        }
        std::cout << ": " << std::endl;

        if (std::cin >> str)
        {
            if ( str.length() < min_size )
            {
                std::cout << "Wrong size, try again";
            }
            else
            {
                break;
            }
        }
        else
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            std::cout << "Wrong input, try again";
        }
    }while (true);

    return str;

}