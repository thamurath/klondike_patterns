#include "CardView.hpp"

#include <sstream>

CardView::CardView(std::string figure, std::string suit, std::string color, std::string face)
: figure(figure)
, suit(suit)
, color(color)
, face(face)
{

}

void CardView::Figure(std::string figure)
{
    this->figure = figure;
}
void CardView::Suit(std::string suit)
{
    this->suit = suit;
}

void CardView::Color(std::string color)
{
    this->color = color;
}
void CardView::Face(std::string face)
{
    this->face = face;
}

std::string const CardView::Figure(void) const
{
    return figure;
}
std::string const CardView::Suit(void) const
{
    return suit;
}

std::string const CardView::Color(void) const
{
    return color;
}

std::string const CardView::Face(void) const
{
    return face;
}


std::string to_string(CardView const& cardView)
{
    std::stringstream ss;
    ss << "figure:" << cardView.Figure()
       << " suit:" << cardView.Suit()
       << " color:" << cardView.Color()
       << " face:" << cardView.Face();
    return ss.str();
}
