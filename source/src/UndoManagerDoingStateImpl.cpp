#include "UndoManagerDoingStateImpl.hpp"
#include "UndoManagerContext.hpp"
#include "UndoManagerUndoingRedoingStateImpl.hpp"
#include "ModelState.hpp"

#include <cassert>

namespace undo_manager
{
    DoingStateImpl::DoingStateImpl(Context &context)
            : StateImpl(context)
            , shouldChange(false)
    {

    }

    void DoingStateImpl::to_do(std::unique_ptr<ModelState> modelState)
    {
        getContext().StatePool().push_back(std::move(modelState));
    }

    ModelState const& DoingStateImpl::to_undo(void)
    {
        shouldChange = true;
        getContext().Mark() = getContext().StatePool().rbegin();
        ++(getContext().Mark());
        return *(*(getContext().Mark()));
    }

    ModelState const& DoingStateImpl::to_redo(void)
    {
        assert(false);
    }

    void DoingStateImpl::updateCurrentState(void) const
    {
        if (shouldChange)
        {
            getContext().CurrentState().Reset(std::make_unique<UndoingRedoingStateImpl>(getContext()));
        }

        shouldChange = false;
    }
}

