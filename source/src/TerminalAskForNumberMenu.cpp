#include "TerminalAskForNumberMenu.hpp"
#include <iostream>


std::int64_t TerminalAskForNumberMenu::Execute(std::string questionLabel)
{
    std::int64_t num(0);
    do{
        std::cout << '\n' << questionLabel <<  "(min:" << min << ", max:" << max << "): " << std::endl;
        if (std::cin >> num)
        {
            if ( (num < min) || (num > max) )
            {
                std::cout << "Wrong limits, try again";
            }
            else
            {
                break;
            }
        }
        else
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            std::cout << "Wrong input, try again";
        }
    }while (true);

    return num;

}