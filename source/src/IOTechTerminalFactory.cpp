#include "IOTechRegisterProxy.hpp"
#include "IOTechTerminalFactory.hpp"

#include "TerminalNumberedOptionMenu.hpp"
#include "TerminalAskForNumberMenu.hpp"
#include "TerminalAskForStringMenu.hpp"

static IOTechRegisterProxy<IOTechTerminalFactory> autoRegisterProxy;

std::unique_ptr<NumberedOptionMenu> IOTechTerminalFactory::GetNumberedOptionMenu(std::vector<concepts::stringable>&& options)
{
    return std::make_unique<TerminalNumberedOptionMenu>(std::forward<std::decay_t<decltype(options)>>(options));
}

std::string IOTechTerminalFactory::GetAbstractFactoryType(void)
{
    return "terminal";
}

std::unique_ptr<AskForNumberMenu> IOTechTerminalFactory::GetAskForNumberMenu(std::int64_t min, std::int64_t max) {
    return std::make_unique<TerminalAskForNumberMenu>(min, max);
}

std::unique_ptr<AskForStringMenu> IOTechTerminalFactory::GetAskForStringMenu(uint32_t min_size) {
    return std::make_unique<TerminalAskForStringMenu>(min_size);
}
