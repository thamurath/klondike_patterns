#ifndef KLONDIKE_DECKTOWASTE_COMMANDBUILDER_HPP
#define KLONDIKE_DECKTOWASTE_COMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class DeckToWasteCommandBuilder final : public CommandBuilder
{
public:
    using CommandBuilder::CommandBuilder;

    ~DeckToWasteCommandBuilder() override = default;

    //avoid copy
    DeckToWasteCommandBuilder(DeckToWasteCommandBuilder const& rhs) = delete;
    DeckToWasteCommandBuilder& operator=(DeckToWasteCommandBuilder const& rhs) = delete;

    //avoid movement
    DeckToWasteCommandBuilder(DeckToWasteCommandBuilder&& rhs) = delete;
    DeckToWasteCommandBuilder& operator=(DeckToWasteCommandBuilder&& rhs) = delete;

    std::unique_ptr<Command> Build(TableView &tableView) override;

    static std::string Description(void)
    {
        return "Deck to Waste";
    }
protected:
private:


};



#endif //KLONDIKE_DECKTOWASTE_COMMANDBUILDER_HPP
