#ifndef KLONDIKE_IOTECHFACTORY_HPP
#define KLONDIKE_IOTECHFACTORY_HPP

#include "NumberedOptionMenu.hpp"
#include "AskForNumberMenu.hpp"
#include "AskForStringMenu.hpp"

#include "stringable.hpp"

#include <vector>
#include <string>
#include <memory>

class IOTechAbstractFactory
{
public:
    IOTechAbstractFactory(void) = default;
    virtual ~IOTechAbstractFactory(void) = default;

    virtual std::unique_ptr<NumberedOptionMenu> GetNumberedOptionMenu(std::vector<concepts::stringable>&& options) = 0;
    virtual std::unique_ptr<AskForNumberMenu> GetAskForNumberMenu(std::int64_t min, std::int64_t max) = 0;
    virtual std::unique_ptr<AskForStringMenu> GetAskForStringMenu(std::uint32_t min_size = 0) = 0;

protected:
private:


};



#endif //KLONDIKE_IOTECHFACTORY_HPP
