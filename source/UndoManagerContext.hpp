#ifndef KLONDIKE_PATTERNS_UNDOMANAGERCONTEXT_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERCONTEXT_HPP

#include "ModelState.hpp"
#include "UndoManagerState.hpp"

#include <deque>
#include <memory>

namespace  undo_manager
{
    class Context final
    {
    public:
        explicit Context(std::unique_ptr<StateImpl> &&state);
        ~Context() = default;

        Context(Context const& rhs ) = delete;
        Context& operator=(Context const& rhs ) = delete;

        Context(Context&& rhs ) noexcept = delete;
        Context& operator=(Context&& rhs ) noexcept = delete;

        void SetInitialState(std::unique_ptr<ModelState> initialState);

        using StatePoolType = std::deque<std::unique_ptr<ModelState>>;
        StatePoolType& StatePool(void);
        StatePoolType::reverse_iterator& Mark(void);

        State& CurrentState(void);

    private:

        StatePoolType statePool;
        StatePoolType::reverse_iterator mark;
        std::unique_ptr<State> currentState;
    };
}
#endif //KLONDIKE_PATTERNS_UNDOMANAGERCONTEXT_HPP
