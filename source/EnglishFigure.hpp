#ifndef KLONDIKE_PATTERNS_ENGLISHFIGURE_HPP
#define KLONDIKE_PATTERNS_ENGLISHFIGURE_HPP

#include "smart_enum.hpp"
#include "Figure.hpp"

namespace english_card
{
    class EnglishFigure final : public smart_enum, public Figure
    {
    public:
        using smart_enum::smart_enum;
        // unique objects for this type
        static const EnglishFigure AS;
        static const EnglishFigure TWO;
        static const EnglishFigure THREE;
        static const EnglishFigure FOUR;
        static const EnglishFigure FIVE;
        static const EnglishFigure SIX;
        static const EnglishFigure SEVEN;
        static const EnglishFigure EIGHT;
        static const EnglishFigure NINE;
        static const EnglishFigure TEN;
        static const EnglishFigure JACK;
        static const EnglishFigure QUEEN;
        static const EnglishFigure KING;
    };

    const EnglishFigure EnglishFigure::AS(1,"AS");
    const EnglishFigure EnglishFigure::TWO(2,"TWO");
    const EnglishFigure EnglishFigure::THREE(3,"THREE");
    const EnglishFigure EnglishFigure::FOUR(4,"FOUR");
    const EnglishFigure EnglishFigure::FIVE(5,"FIVE");
    const EnglishFigure EnglishFigure::SIX(6,"SIX");
    const EnglishFigure EnglishFigure::SEVEN(7,"SEVEN");
    const EnglishFigure EnglishFigure::EIGHT(8,"EIGHT");
    const EnglishFigure EnglishFigure::NINE(9,"NINE");
    const EnglishFigure EnglishFigure::TEN(10,"TEN");
    const EnglishFigure EnglishFigure::JACK(11,"JACK");
    const EnglishFigure EnglishFigure::QUEEN(12,"QUEEN");
    const EnglishFigure EnglishFigure::KING(13,"KING");
}
#endif //KLONDIKE_PATTERNS_ENGLISHFIGURE_HPP
