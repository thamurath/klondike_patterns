#ifndef KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXY_HPP
#define KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXY_HPP

#include "CommandBuilderRegisterProxyBase.hpp"
#include "CommandBuilder.hpp"

#include <memory>
template <typename ConcreteCommandBuilder>
class CommandBuilderRegisterProxy : public CommandBuilderRegisterProxyBase
{
public:
    std::unique_ptr<CommandBuilder> CreateCommandBuilder(void) const override
    {
        return std::make_unique<ConcreteCommandBuilder>();
    }

    std::string str(void) const override
    {
        return ConcreteCommandBuilder::Description();
    }
};

#endif //KLONDIKE_PATTERNS_COMMANDBUILDERREGISTERPROXY_HPP
