#ifndef KLONDIKE_PATTERNS_ELEMENTTYPE_HPP
#define KLONDIKE_PATTERNS_ELEMENTTYPE_HPP

#include <better_enums.hpp>
#include <cstdint>

BETTER_ENUM(ElementType, std::uint8_t,
        waste,
        pile,
        deck,
        foundation
);

#endif //KLONDIKE_PATTERNS_ELEMENTTYPE_HPP
