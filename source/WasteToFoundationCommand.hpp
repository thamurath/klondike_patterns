#ifndef KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMAND_HPP
#define KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMAND_HPP

#include "Command.hpp"

#include "FoundationView.hpp"
#include "WasteView.hpp"

class WasteToFoundationCommand : public Command
{
public:
    WasteToFoundationCommand(WasteView& wasteView, FoundationView& foundationView);
    ~WasteToFoundationCommand(void) override = default;

    ReturnCode Check(TableModel &modelInterface) override;
    ReturnCode Execute(TableModel &modelInterface) override;
    std::vector<ElementView *> GetInvolvedElements(void) const override;

private:
    WasteView& wasteView;
    FoundationView& foundationView;
};

#endif //KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMAND_HPP
