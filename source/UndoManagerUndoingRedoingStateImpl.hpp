#ifndef KLONDIKE_PATTERNS_UNDOMANAGERUNDOINGREDOINGSTATEIMPL_HPP
#define KLONDIKE_PATTERNS_UNDOMANAGERUNDOINGREDOINGSTATEIMPL_HPP

#include "UndoManagerDoingStateImpl.hpp"

namespace undo_manager
{
    class UndoingRedoingStateImpl : public StateImpl
    {
    public:
        explicit UndoingRedoingStateImpl(Context& context);
        ~UndoingRedoingStateImpl() override = default;

        // avoid copy
        UndoingRedoingStateImpl(UndoingRedoingStateImpl const& rhs) = delete;
        UndoingRedoingStateImpl& operator=(UndoingRedoingStateImpl const& rhs) = delete;

        // avoid movement
        UndoingRedoingStateImpl(UndoingRedoingStateImpl&& rhs) noexcept = delete;
        UndoingRedoingStateImpl& operator=(UndoingRedoingStateImpl&& rhs) noexcept = delete;

        void to_do(std::unique_ptr<ModelState> modelState) override ;
        ModelState const& to_undo(void) override ;
        ModelState const& to_redo(void) override ;

        void updateCurrentState(void) const override ;
    private:
        mutable bool shouldChange;
    };
}

#endif //KLONDIKE_PATTERNS_UNDOMANAGERUNDOINGREDOINGSTATEIMPL_HPP
