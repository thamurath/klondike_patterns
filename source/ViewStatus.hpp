#ifndef KLONDIKE_STATUS_HPP
#define KLONDIKE_STATUS_HPP



class ViewStatus final
{
public:
    ViewStatus(void) = default;
    ~ViewStatus(void) = default;
    
    //avoid copy
    ViewStatus(ViewStatus const& rhs) = delete;
    ViewStatus& operator=(ViewStatus const& rhs) = delete;
    
    //avoid movement
    ViewStatus(ViewStatus&& rhs) = delete;
    ViewStatus& operator=(ViewStatus&& rhs) = delete;
protected:
private:
};



#endif //KLONDIKE_STATUS_HPP
