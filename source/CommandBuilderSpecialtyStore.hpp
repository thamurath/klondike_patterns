#ifndef KLONDIKE_PATTERNS_COMMANDBUILDERSPECIALTYSTORE_HPP
#define KLONDIKE_PATTERNS_COMMANDBUILDERSPECIALTYSTORE_HPP

#include <vector>

class CommandBuilderRegisterProxyBase;

class CommandBuilderSpecialtyStore
{
public:
    static CommandBuilderSpecialtyStore& Instance(void);

    void Register(CommandBuilderRegisterProxyBase* proxy);

    auto GetRegisteredElements(void) const
    {
        return registeredCommandBuilders;
    }

private:
    CommandBuilderSpecialtyStore(void) = default;
    std::vector<CommandBuilderRegisterProxyBase const*> registeredCommandBuilders;
};

#endif //KLONDIKE_PATTERNS_COMMANDBUILDERSPECIALTYSTORE_HPP
