#ifndef KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMANDBUILDER_HPP
#define KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class WasteToFoundationCommandBuilder : public CommandBuilder
{
public:
    ~WasteToFoundationCommandBuilder(void) override = default;

    std::unique_ptr<Command> Build(TableView &tableView) override;


    static std::string Description(void)
    {
        return "Waste to Foundation";
    }
};
#endif //KLONDIKE_PATTERNS_WASTETOFOUNDATIONCOMMANDBUILDER_HPP
