#ifndef KLONDIKE_PATTERNS_PILETOPILECOMMANDBUILDER_HPP
#define KLONDIKE_PATTERNS_PILETOPILECOMMANDBUILDER_HPP

#include "CommandBuilder.hpp"

class PileToPileCommandBuilder : public CommandBuilder
{
public:
    ~PileToPileCommandBuilder(void) override = default;

    std::unique_ptr<Command> Build(TableView &tableView) override;


    static std::string Description(void)
    {
        return "Pile to Pile";
    }
};


#endif //KLONDIKE_PATTERNS_PILETOPILECOMMANDBUILDER_HPP
